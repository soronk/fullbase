//package BossBarHouse;
//
//import static org.junit.Assert.*;
//
//public class OrderTest {
//
//    Dish firstDish = new Dish(10d, "firstDish", "first dish");
//    Dish secondDish = new Dish(15d, "firstDish", "first dish");
//    Dish thirdDish = new Dish(150d, "thirdDish", "third dish");
//    Dish fourthDish = new Dish(20d, "fourthDish", "fourth dish");
//    Dish fifthDish = new Dish(5d, "fifthDish", "fifth dish");
//    Dish[] dishes = new Dish[]{firstDish, secondDish, thirdDish, fourthDish, fifthDish};
//    Order order = new Order(dishes);
//
//
//    @org.junit.Test
//    public void addDish() {
//        Dish sixthDish = new Dish(15d, "sixthDish", "sixth dish");
//        order.addDish(sixthDish);
//        Order actual = order;
//        Order expected = new Order(new Dish[]{firstDish, secondDish, thirdDish, fourthDish, fifthDish, sixthDish});
//        assertArrayEquals(expected, actual);
//    }
//
//
//    @org.junit.Test
//    public void removeOneDishWithThisName() {
//        order.removeOneDishWithThisName("firstDish");
//        Order actual = order;
//        Order expected = new Order(new Dish[]{secondDish, thirdDish, fourthDish, fifthDish, null});
//        assertArrayEquals(expected, actual);
//    }
//
//    @org.junit.Test
//    public void removeAllDishWithThisName() {
//        order.removeAllDishWithThisName("firstDish");
//        Order actual = order;
//        Order expected = new Order(Ordernew Dish[]{thirdDish, fourthDish, fifthDish, null, null});
//        assertArrayEquals(expected, actual);
//    }
//
//    @org.junit.Test
//    public void amountDishes() {
//        int actual = order.amountDishes();
//        int expected = 5;
//        assertEquals(expected,actual);
//    }
//
//    @org.junit.Test
//    public void arrayOfDishes() {
//        Dish[] actual = order.arrayOfDishes(dishes);
//        Dish[] expected = new Dish[]{firstDish, secondDish, thirdDish, fourthDish, fifthDish};
//        assertArrayEquals(expected, actual);
//    }
//
//
//
//    @org.junit.Test
//    public void getFullPrice() {
//        double actual = order.getFullPrice();
//        double expected = 200d;
//        double delta = 0;
//        assertEquals(expected,actual,delta);
//    }
//
//    @org.junit.Test
//    public void amountDishesWithThisName() {
//        int actual = order.amountDishesWithThisName("firstDish");
//        int expected = 2;
//        assertEquals(expected,actual);
//    }
//
//    @org.junit.Test
//    public void namesOfDishes() {
//        String[] actual = order.namesOfDishes();
//        String[] expected = {"firstDish", "thirdDish", "fourthDish", "fifthDish"};
//        assertArrayEquals(expected,actual);
//    }
//
//
//
//    @org.junit.Test
//    public void sortDishesOfPrice() {
//        Dish[] actual = order.sortDishesOfPrice();
//        Dish[] expected = new Dish[]{thirdDish, fourthDish, secondDish, firstDish, fifthDish};
//        assertArrayEquals(expected,actual);
//    }
//
//    private void assertArrayEquals(Order expected, Order actual) {
//    }
//
//    private void assertArrayEquals(Dish[] expected, Dish[] actual) {
//    }
//
//    private void assertArrayEquals(String[] expected, String[] actual) {
//    }
//}
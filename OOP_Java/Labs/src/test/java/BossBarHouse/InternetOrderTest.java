package BossBarHouse;

import org.junit.Test;

import static org.junit.Assert.*;

public class InternetOrderTest {

    @Test
    public void add() {
    }

    @Test
    public void remove() {
    }

    @Test
    public void remove1() {
    }

    @Test
    public void removeAll() {
    }

    @Test
    public void removeAll1() {
    }

    @Test
    public void itemQuantity() {
    }

    @Test
    public void getItems() {
    }

    @Test
    public void costTotal() {
    }

    @Test
    public void itemQuantity1() {
        Dish item1 = new Dish("name1","2");
        Dish item2 = new Dish("name1","1");
        Dish item3 = new Dish("name3","0");
        Dish item4 = new Dish("name3","0");
        MenuItem[] menuItems = new MenuItem[]{item1,item2,item3,item4} ;
        Customer customer = new Customer();
        InternetOrder internetOrder = new InternetOrder(menuItems,customer);
        int expected = internetOrder.itemQuantity("name1");
        assertEquals(expected,2);
    }

    @Test
    public void itemQuantity2() {
    }

    @Test
    public void itemsNames() {
    }

    @Test
    public void sortedItemsByCostDesc() {
    }

    @Test
    public void getCustomer() {
    }

    @Test
    public void setCustomer() {
    }
}
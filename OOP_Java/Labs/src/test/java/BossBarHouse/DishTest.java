//package BossBarHouse;
//
//import static org.junit.Assert.*;
//
//public class DishTest {
//
//
//    Dish backDish = new Dish("name","description");
//    Dish dish = new Dish(10d, "name","description" );
//
//    @org.junit.Test
//    public void getBackDishPrice() {
//        double actual = backDish.getPrice();
//        double expected  = 0;
//        double delta = 0;
//        assertEquals(expected,actual, delta);
//    }
//
//    @org.junit.Test
//    public void getPrice() {
//        double actual = dish.getPrice();
//        double expected  = 10d;
//        double delta = 0;
//        assertEquals(expected,actual, delta);
//    }
//
//    @org.junit.Test
//    public void setPrice() {
//        dish.setPrice(11d);
//        double actual = dish.getPrice();
//        double expected = 11d;
//        double delta = 0;
//        assertEquals(expected,actual, delta);
//
//    }
//
//    @org.junit.Test
//    public void getDishName() {
//        String actual = dish.getDishName();
//        String expected = "name";
//        assertEquals(expected,actual);
//    }
//
//    @org.junit.Test
//    public void setDishName() {
//        dish.setDishName("newName");
//        String actual = dish.getDishName();
//        String expected = "newName";
//        assertEquals(expected,actual);
//    }
//
//    @org.junit.Test
//    public void getDishDescription() {
//        String actual = dish.getDishDescription();
//        String expected = "description";
//        assertEquals(expected,actual);
//    }
//
//    @org.junit.Test
//    public void setDishDescription() {
//        dish.setDishDescription("New description");
//        String actual = dish.getDishDescription();
//        String expected = "New description";
//        assertEquals(expected,actual);
//    }
//}
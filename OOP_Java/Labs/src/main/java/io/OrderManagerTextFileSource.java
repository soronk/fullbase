package io;

import BossBarHouse.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OrderManagerTextFileSource extends OrderManagerFileSource {

    public OrderManagerTextFileSource(String path){
        setPath(path);
    }


    @Override
    public void load(Order order) {
        Scanner scanner = new Scanner(path(order));
        LocalDate birthDate = LocalDate.ofEpochDay(Long.parseLong(scanner.next()));
        String firstName = scanner.nextLine();
        String secondName = scanner.nextLine();
        String cityName = scanner.nextLine();
        int zipCode = scanner.nextInt();
        String streetName = scanner.nextLine();
        int buildingNumber = scanner.nextInt();
        char buildingLetter = scanner.nextLine().charAt(0);
        int apartmentNumber = scanner.nextInt();
        int size = scanner.nextInt();
        List<MenuItem> menuItems = new ArrayList<>(size);
        String name;
        String description;
        String drinkType;
        double cost;
        double alcoholVolume;

        for (int i = 0; i < size ; i++) {
            name = scanner.nextLine();
            description = scanner.nextLine();
            cost = scanner.nextInt();
            if(scanner.nextLine().equals("Drink")){
                alcoholVolume = scanner.nextDouble();
                drinkType = scanner.nextLine();
                menuItems.add(new Drink(cost,name,description, DrinkTypeEnum.valueOf(drinkType), alcoholVolume));
            } else {
                menuItems.add(new Dish(cost,name,description));
            }
        }

        Address address = new Address(cityName,zipCode,streetName,buildingNumber,buildingLetter,apartmentNumber);
        Customer customer = new Customer(firstName,secondName,birthDate,address);
        scanner.close();
        order.clear();
        order.setCustomer(customer);
        order.addAll(menuItems);
    }

    @Override
    public void store(Order order) {
        try {
            PrintWriter writer = new PrintWriter(path(order), "UTF-8");
            writer.println(order.getCustomer().getBirthDate());
            writer.println(order.getCustomer().getFirstName());
            writer.println(order.getCustomer().getSecondName());
            writer.println(order.getCustomer().getAddress().getCityName());
            writer.println(order.getCustomer().getAddress().getZipCode());
            writer.println(order.getCustomer().getAddress().getStreetName());
            writer.println(order.getCustomer().getAddress().getBuildingNumber());
            writer.println(order.getCustomer().getAddress().getBuildingLetter());
            writer.println(order.getCustomer().getAddress().getApartmentNumber());
            for(MenuItem menuItem : order){
                writer.println(menuItem.getName());
                writer.println(menuItem.getDescription());
                writer.println(menuItem.getCost());
                if(menuItem instanceof Drink){
                    writer.println("Drink");
                    writer.println(((Drink) menuItem).getAlcoholVol());
                }else{
                    writer.println("Dish");
                }
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Order order) {
        try {
            Files.delete(Paths.get(path(order)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(Order order) {
        try {
            Files.createFile(Paths.get(path(order)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String path(Order order) {
        return getPath() + order.getLocalDateTime() + ".txt";
    }
}

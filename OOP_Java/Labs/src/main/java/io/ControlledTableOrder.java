package io;

import BossBarHouse.*;

import java.time.LocalDateTime;
import java.util.Collection;

public class ControlledTableOrder extends TableOrder {

    protected boolean isChanged = false;

    public ControlledTableOrder() {
        super();
    }

    public ControlledTableOrder(int size, Customer customer) {
        super(size, customer);
    }

    public ControlledTableOrder(MenuItem[] menuItems, Customer customer) {
        super(menuItems, customer);
    }

    public ControlledTableOrder(Order order) {
        Order currentOrder = order;
    }

    protected boolean isChanged() {
        return isChanged;
    }

    @Override
    public boolean add(MenuItem menuItem) {
        isChanged = true;
        return super.add(menuItem);
    }

    @Override
    public boolean remove(String menuItemName) {
        isChanged = true;
        return super.remove(menuItemName);
    }

    @Override
    public int removeAll(String menuItemName) {
        isChanged = true;
        return super.removeAll(menuItemName);
    }

    @Override
    public void setCustomer(Customer customer) {
        isChanged = true;
        super.setCustomer(customer);
    }

    @Override
    public void setLocalDateTime(LocalDateTime localDateTime) {
        isChanged = true;
        super.setLocalDateTime(localDateTime);
    }

    @Override
    public MenuItem set(int index, MenuItem element) {
        isChanged = true;
        return super.set(index, element);
    }

    @Override
    public MenuItem remove(int index) {
        isChanged = true;
        return super.remove(index);
    }

    @Override
    public boolean addAll(Collection<? extends MenuItem> c) {
        isChanged = true;
        return super.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends MenuItem> c) {
        isChanged = true;
        return super.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        isChanged = true;
        return super.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        isChanged = true;
        return super.retainAll(c);
    }

    @Override
    public void clear() {
        isChanged = true;
        super.clear();
    }
}

package io;

import BossBarHouse.MenuItem;
import BossBarHouse.Order;
import BossBarHouse.TableOrderManager;

import java.util.Collection;

public class ControlledTableOrderManager extends TableOrderManager {

    protected Source<Order> source;

    public ControlledTableOrderManager(int numberTables) {
        super(numberTables);
    }

    public Source<Order> getSource() {
        return source;
    }

    public void setSource(Source<Order> source) {
        this.source = source;
    }

    @Override
    public void add(int numberOfTable, Order order) {
        ControlledTableOrder controlledTableOrder = new ControlledTableOrder(order);
        source.create(order);
        super.add(numberOfTable, order);
    }

    @Override
    public void addItem(int numberOfTable, MenuItem menuItem) {
        super.addItem(numberOfTable, menuItem);
    }

    @Override
    public Order remove(int numberOfTable) {
        Order order = super.get(numberOfTable);
        source.delete(order);
        return super.remove(numberOfTable);
    }

    @Override
    public boolean add(Order order) {
        ControlledTableOrder controlledTableOrder = new ControlledTableOrder(order);
        source.create(order);
        return super.add(order);
    }

    @Override
    public boolean remove(Object o) {
        source.delete((Order) o);
        return super.remove(o);
    }

    @Override
    public boolean addAll(Collection<? extends Order> c) {
        for (Order order : c) {
            ControlledTableOrder controlledTableOrder = new ControlledTableOrder(order);
            source.create(order);
        }
        return super.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends Order> c) {
        for (Order order : c) {
            ControlledTableOrder controlledTableOrder = new ControlledTableOrder(order);
            source.create(order);
        }
        return super.addAll(index, c);
    }

    @Override
    public Order set(int index, Order element) {
        return super.set(index, element);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object o : c) {
            source.delete((Order) o);
        }
        return super.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (Object o : c) {
            if (!super.contains(o)) {
                source.delete((Order) o);
            }
        }
        return super.retainAll(c);
    }

    void load(Order order) {
        source.load(order);
    }

    void store(Order order) {
        ControlledTableOrder controlledTableOrder = new ControlledTableOrder(order);
        if(controlledTableOrder.isChanged()){
            source.store(order);
        }
    }


}

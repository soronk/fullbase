package io;

import BossBarHouse.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class OrderManagerBinaryFileSource extends OrderManagerFileSource {

    public OrderManagerBinaryFileSource(String path){
        setPath(path);
    }

    @Override
    public void load(Order order) {
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(path(order)));
            LocalDate birthDate = LocalDate.ofEpochDay(dataInputStream.readLong());
            String firstName = dataInputStream.readUTF();
            String secondName = dataInputStream.readUTF();
            String cityName = dataInputStream.readUTF();
            int zipCode = dataInputStream.readInt();
            String streetName = dataInputStream.readUTF();
            int buildingNumber = dataInputStream.readInt();
            char buildingLetter = dataInputStream.readChar();
            int apartmentNumber = dataInputStream.readInt();
            int size = dataInputStream.readInt();
            List<MenuItem> menuItems = new ArrayList<>(size);
            String name;
            String description;
            String drinkType;
            double cost;
            double alcoholVolume;
            for (int i = 0; i < size; i++) {
                name = dataInputStream.readUTF();
                description = dataInputStream.readUTF();
                cost = dataInputStream.readInt();
                if (dataInputStream.readUTF().equals("Drink")) {
                    alcoholVolume = dataInputStream.readDouble();
                    drinkType = dataInputStream.readUTF();
                    menuItems.add(new Drink(cost, name, description, DrinkTypeEnum.valueOf(drinkType), alcoholVolume));
                } else {
                    menuItems.add(new Dish(cost, name, description));
                }
            }

            Address address = new Address(cityName, zipCode, streetName, buildingNumber, buildingLetter, apartmentNumber);
            Customer customer = new Customer(firstName, secondName, birthDate, address);
            order.clear();
            order.setCustomer(customer);
            order.addAll(menuItems);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void store(Order order) {
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(path(order)));
            dataOutputStream.writeLong(order.getCustomer().getBirthDate().toEpochDay());
            dataOutputStream.writeUTF(order.getCustomer().getFirstName());
            dataOutputStream.writeUTF(order.getCustomer().getSecondName());
            dataOutputStream.writeUTF(order.getCustomer().getAddress().getCityName());
            dataOutputStream.writeInt(order.getCustomer().getAddress().getZipCode());
            dataOutputStream.writeUTF(order.getCustomer().getAddress().getStreetName());
            dataOutputStream.writeInt(order.getCustomer().getAddress().getBuildingNumber());
            dataOutputStream.writeChar(order.getCustomer().getAddress().getBuildingLetter());
            dataOutputStream.writeInt(order.getCustomer().getAddress().getApartmentNumber());

            for (MenuItem menuItem : order) {
                dataOutputStream.writeUTF(menuItem.getName());
                dataOutputStream.writeUTF(menuItem.getDescription());
                dataOutputStream.writeDouble(menuItem.getCost());
                if (menuItem instanceof Drink) {
                    dataOutputStream.writeUTF("Drink");
                    dataOutputStream.writeDouble(((Drink) menuItem).getAlcoholVol());
                    dataOutputStream.writeUTF(((Drink) menuItem).getDrinkType().toString());
                } else {
                    dataOutputStream.writeUTF("Dish");
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Order order) {
        try {
            Files.delete(Paths.get(path(order)));
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void create(Order order) {
        try {
            Files.createFile(Paths.get(path(order)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        store(order);
    }

    private String path(Order order) {
        return getPath() + order.getLocalDateTime().toEpochSecond(ZoneOffset.ofHours(4)) + ".bin";
    }

}

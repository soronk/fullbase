package io;

public abstract class OrderManagerFileSource implements FileSorce {
    private String path;

    @Override
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String getPath() {
        return path;
    }
}

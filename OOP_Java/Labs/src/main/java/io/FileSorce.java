package io;

import BossBarHouse.Order;

public interface FileSorce extends Source<Order> {
    public void setPath(String path);
    public String getPath();
}

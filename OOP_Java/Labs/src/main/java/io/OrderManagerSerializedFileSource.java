package io;

import BossBarHouse.Order;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class OrderManagerSerializedFileSource extends OrderManagerFileSource {

    public OrderManagerSerializedFileSource(String path){
        setPath(path);
    }

    @Override
    public void load(Order order) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(path(order)));
            Order readerOrder = (Order) in.readObject();
            order.clear();
            order.addAll(readerOrder);
            order.setCustomer(readerOrder.getCustomer());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void store(Order order) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path(order)));
            out.writeObject(order);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Order order) {
        try {
            Files.delete(Paths.get(getPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(Order order) {
        store(order);
    }

    private String path(Order order){
        return order.getLocalDateTime() + ".txt";
    }

}

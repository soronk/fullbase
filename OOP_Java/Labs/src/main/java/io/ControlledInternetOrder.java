package io;

import BossBarHouse.Customer;
import BossBarHouse.InternetOrder;
import BossBarHouse.MenuItem;
import BossBarHouse.Order;

import java.time.LocalDateTime;
import java.util.Collection;

public class ControlledInternetOrder extends InternetOrder {

    protected boolean isChanged = false;

    public ControlledInternetOrder(){
        super();
    }

    public ControlledInternetOrder(MenuItem[] menuItems, Customer customer){
        super(menuItems, customer);
    }

    public ControlledInternetOrder(Order order) {
        Order currentOrder = order;
    }

    @Override
    public boolean add(MenuItem menuItem) {
        isChanged = true;
        return super.add(menuItem);
    }

    @Override
    public boolean remove(String name) {
        isChanged = true;
        return super.remove(name);
    }

    @Override
    public boolean remove(Object menuItem) {
        isChanged = true;
        return super.remove(menuItem);
    }

    @Override
    public int removeAll(String name) {
        isChanged = true;
        return super.removeAll(name);
    }

    @Override
    public int removeAll(MenuItem menuItem) {
        isChanged = true;
        return super.removeAll(menuItem);
    }

    @Override
    public void setCustomer(Customer customer) {
        isChanged = true;
        super.setCustomer(customer);
    }

    @Override
    public void setLocalDateTime(LocalDateTime localDateTime) {
        isChanged = true;
        super.setLocalDateTime(localDateTime);
    }

    @Override
    public MenuItem set(int index, MenuItem element) {
        isChanged = true;
        return super.set(index, element);
    }

    @Override
    public void add(int index, MenuItem element) {
        isChanged = true;
        super.add(index, element);
    }

    @Override
    public MenuItem remove(int index) {
        isChanged = true;
        return super.remove(index);
    }

    @Override
    public boolean addAll(Collection<? extends MenuItem> c) {
        isChanged = true;
        return super.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends MenuItem> c) {
        isChanged = true;
        return super.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        isChanged = true;
        return super.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        isChanged = true;
        return super.retainAll(c);
    }

    @Override
    public void clear() {
        isChanged = true;
        super.clear();
    }

    protected boolean isChanged() {
        return isChanged;
    }
}

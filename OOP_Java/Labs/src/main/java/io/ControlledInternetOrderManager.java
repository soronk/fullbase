package io;

import BossBarHouse.AlreadyAddedException;
import BossBarHouse.InternetOrdersManager;
import BossBarHouse.Order;

import java.util.Collection;

public class ControlledInternetOrderManager extends InternetOrdersManager {

    protected Source<Order> source;

    public ControlledInternetOrderManager(){
        super();
    }

    public ControlledInternetOrderManager(Order[] orders) throws AlreadyAddedException {
        super(orders);
    }

    @Override
    public boolean add(Order order) {
        source.create(order);
        return super.add(order);
    }

    @Override
    public Order remove() {
        InternetOrdersManager internetOrdersManager = new InternetOrdersManager();
        source.delete(internetOrdersManager.getFirst());
        return super.remove();
    }

    @Override
    public boolean addAll(Collection<? extends Order> c) {
        for (Order order : c ){
            ControlledInternetOrder controlledInternetOrder = new ControlledInternetOrder(order);
            source.create(order);
        }
        return super.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for(Object order : c){
            source.delete((Order) order);
        }
        return super.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (Object order : c){
            if(!super.contains(order)){
                source.delete((Order) order);
            }
        }
        return super.retainAll(c);
    }

    public Source<Order> getSource() {
        return source;
    }

    public void setSource(Source<Order> source) {
        this.source = source;
    }

    void load(Order order) {
        source.load(order);
    }

    void store(Order order) {
        ControlledInternetOrder controlledInternetOrder = new ControlledInternetOrder(order);
        if(controlledInternetOrder.isChanged()){
            source.store(order);
        }
    }

}

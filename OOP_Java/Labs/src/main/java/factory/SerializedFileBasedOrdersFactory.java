package factory;

import BossBarHouse.*;
import io.*;

public class SerializedFileBasedOrdersFactory extends OrdersFactory {

    private String path;

    public SerializedFileBasedOrdersFactory(String path) {
        this.path = path;
    }

    @Override
    public Order createTableOrder() {
        return new ControlledTableOrder();
    }

    @Override
    public Order createTableOrder(int capacity, Customer customer) {
        return new ControlledTableOrder(capacity,customer);
    }

    @Override
    public Order createTableOrder(Customer customer, MenuItem[] menuItems) {
        return new ControlledTableOrder(menuItems,customer);
    }

    @Override
    public Order createInternetOrder() {
        return new ControlledInternetOrder();
    }

    @Override
    public Order createInternetOrder(MenuItem[] menuItems, Customer customer) {
        return new ControlledInternetOrder(menuItems,customer);
    }

    @Override
    public OrdersManager createTableOrderManager(int capacity) {
        OrderManagerSerializedFileSource orderManagerSerializedFileSource = new OrderManagerSerializedFileSource(path);
        ControlledTableOrderManager controlledTableOrderManager = new ControlledTableOrderManager(capacity);
        controlledTableOrderManager.setSource(orderManagerSerializedFileSource);
        return controlledTableOrderManager;
    }

    @Override
    public OrdersManager createInternetOrderManager() {
        OrderManagerSerializedFileSource orderManagerSerializedFileSource = new OrderManagerSerializedFileSource(path);
        ControlledInternetOrderManager controlledInternetOrderManager = new ControlledInternetOrderManager();
        controlledInternetOrderManager.setSource(orderManagerSerializedFileSource);
        return controlledInternetOrderManager;
    }

    @Override
    public OrdersManager createInternetOrderManager(Order[] orders) throws AlreadyAddedException {
        OrderManagerSerializedFileSource orderManagerSerializedFileSource = new OrderManagerSerializedFileSource(path);
        ControlledInternetOrderManager controlledInternetOrderManager = new ControlledInternetOrderManager(orders);
        controlledInternetOrderManager.setSource(orderManagerSerializedFileSource);
        return controlledInternetOrderManager;
    }
}

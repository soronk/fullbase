package factory;

import BossBarHouse.*;

public abstract class OrdersFactory {

    public abstract Order createTableOrder();
    public abstract Order createTableOrder(int capacity, Customer customer);
    public abstract Order createTableOrder(Customer customer, MenuItem[] menuItems);
    public abstract Order createInternetOrder();
    public abstract Order createInternetOrder(MenuItem[] menuItems, Customer customer);
    public abstract OrdersManager createTableOrderManager(int capacity);
    public abstract OrdersManager createInternetOrderManager();
    public abstract OrdersManager createInternetOrderManager(Order[] orders) throws AlreadyAddedException;

    static public OrdersFactory getOrderFactory(OrdersFactoryTypesEnumeration type, String path) {
       switch (type){
           case ORDINARY_ORDERS_FACTORY:
               return new OrdinaryOrdersFactory();
           case SOCKET_BASED_ORDERS_FACTORY:
               return null;
           case TEXT_FILE_BASED_ORDERS_FACTORY:
               return null;
           case BINARY_FILE_BASED_ORDERS_FACTORY:
               return null;
           case SERIALIZED_FILE_BASED_ORDERS_FACTORY:
               return null;
       }
        return null;
    }

}

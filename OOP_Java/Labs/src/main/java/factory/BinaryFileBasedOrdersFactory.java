package factory;

import BossBarHouse.*;
import io.*;

public class BinaryFileBasedOrdersFactory extends OrdersFactory {

    private String path;

    public BinaryFileBasedOrdersFactory(String path) {
        this.path = path;
    }

    @Override
    public Order createTableOrder() {
        return new ControlledTableOrder();
    }

    @Override
    public Order createTableOrder(int capacity, Customer customer) {
        return new ControlledTableOrder(capacity,customer);
    }

    @Override
    public Order createTableOrder(Customer customer, MenuItem[] menuItems) {
        return new ControlledTableOrder(menuItems,customer);
    }

    @Override
    public Order createInternetOrder() {
        return new ControlledInternetOrder();
    }

    @Override
    public Order createInternetOrder(MenuItem[] menuItems, Customer customer) {
        return new ControlledInternetOrder(menuItems,customer);
    }

    @Override
    public OrdersManager createTableOrderManager(int capacity) {
        OrderManagerBinaryFileSource orderManagerBinaryFileSource = new OrderManagerBinaryFileSource(path);
        ControlledTableOrderManager controlledTableOrderManager = new ControlledTableOrderManager(capacity);
        controlledTableOrderManager.setSource(orderManagerBinaryFileSource);
        return controlledTableOrderManager;
    }

    @Override
    public OrdersManager createInternetOrderManager() {
        OrderManagerBinaryFileSource orderManagerBinaryFileSource = new OrderManagerBinaryFileSource(path);
        ControlledInternetOrderManager controlledInternetOrderManager = new ControlledInternetOrderManager();
        controlledInternetOrderManager.setSource(orderManagerBinaryFileSource);
        return controlledInternetOrderManager;
    }

    @Override
    public OrdersManager createInternetOrderManager(Order[] orders) throws AlreadyAddedException {
        OrderManagerBinaryFileSource orderManagerBinaryFileSource = new OrderManagerBinaryFileSource(path);
        ControlledInternetOrderManager controlledInternetOrderManager = new ControlledInternetOrderManager(orders);
        controlledInternetOrderManager.setSource(orderManagerBinaryFileSource);
        return controlledInternetOrderManager;
    }
}

package factory;

import BossBarHouse.*;

public class OrdinaryOrdersFactory extends OrdersFactory {
    @Override
    public Order createTableOrder() {
        return new TableOrder();
    }

    @Override
    public Order createTableOrder(int capacity, Customer customer) {
        return new TableOrder(capacity, customer);
    }

    @Override
    public Order createTableOrder(Customer customer, MenuItem[] menuItems) {
        return new TableOrder(menuItems, customer);
    }

    @Override
    public Order createInternetOrder() {
        return new InternetOrder();
    }

    @Override
    public Order createInternetOrder(MenuItem[] menuItems, Customer customer) {
        return new InternetOrder(menuItems, customer);
    }

    @Override
    public OrdersManager createTableOrderManager(int capacity) {
        return new TableOrderManager(capacity);
    }

    @Override
    public OrdersManager createInternetOrderManager() {
        return new InternetOrdersManager();
    }

    @Override
    public OrdersManager createInternetOrderManager(Order[] orders) throws AlreadyAddedException {
        return new InternetOrdersManager(orders);
    }
}

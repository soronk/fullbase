package BossBarHouse;

import java.time.LocalDateTime;
import java.util.List;

public interface Order extends List<MenuItem> {

    //В классы, реализующие интерфейс Order, добавьте поле – время заказа (дата и время).
    //Добавьте в интерфейс методы установки и получения даты заказа. Рекомендуется
    //использовать LocalDateTime. Во всех конструкторах это поле инициализируется
    //текущим временем.

    public boolean add(MenuItem item);//
    public String[] itemsNames();
    public int itemQuantity();
    public int itemQuantity(String itemName);
    public int itemQuantity(MenuItem menuItem);
    public MenuItem[] getItems();
    public boolean remove(String itemName);//
    public boolean remove(Object item);//
    public int removeAll(String itemName);//
    public int removeAll(MenuItem item);//
    public MenuItem[] sortedItemsByCostDesc();
    public double costTotal();
    public Customer getCustomer();
    public void setCustomer(Customer customer);
    public void setLocalDateTime(LocalDateTime localDateTime);
    public LocalDateTime getLocalDateTime();
}

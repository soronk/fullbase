package BossBarHouse;

import java.util.Objects;

public final class Address {

    private final String cityName;
    private final String streetName;
    private final int zipCode;
    private final int buildingNumber;
    private final int apartmentNumber;
    private final char buildingLetter;

    private final static int DEFAULT_ZIP_CODE = -1;
    private final static int DEFAULT_BUILDING_NUMBER = -1;
    private final static int DEFAULT_APARTMENT_NUMBER = -1;
    private final static String DEFAULT_CITY_NAME = "";
    private final static String DEFAULT_STREET_NAME = "";
    private final static char DEFALUT_BUILDING_LETTER = ' ';

    public static final Address EMPTY_ADRESS = new Address();

    public Address() {
        this(DEFAULT_CITY_NAME, DEFAULT_ZIP_CODE, DEFAULT_STREET_NAME, DEFAULT_BUILDING_NUMBER, DEFALUT_BUILDING_LETTER, DEFAULT_APARTMENT_NUMBER);
    }

    public Address(String streetName, int buildingNumber, char buildingLetter, int apartmentNumber) {
        this("Samara", DEFAULT_ZIP_CODE, streetName, buildingNumber, buildingLetter, apartmentNumber);
    }

    public Address(String cityName, int zipCode, String streetName, int buildingNumber, char buildingLetter, int apartmentNumber) {
        this.cityName = cityName;
        this.streetName = streetName;

        if (buildingNumber < 0) {
            throw new IllegalArgumentException("Номер здания не может быть отрицательным числом");
        }
        this.buildingNumber = buildingNumber;

        if (apartmentNumber < 0) {
            throw new IllegalArgumentException("Номер квартиры не может быть отрицательным");
        }
        this.apartmentNumber = apartmentNumber;

        if (zipCode < 0) {
            throw new IllegalArgumentException("Почтовый индекс не может быть отрицательным числом");
        }
        this.zipCode = zipCode;

        if (!Character.isLetter(buildingLetter)) {
            throw new IllegalArgumentException("Литера здания должна быть буквой");
        }
        this.buildingLetter = buildingLetter;
    }

    public String getCityName() {
        return cityName;
    }

    public String getStreetName() {
        return streetName;
    }

    public int getZipCode() {
        return zipCode;
    }

    public int getBuildingNumber() {
        return buildingNumber;
    }

    public int getApartmentNumber() {
        return apartmentNumber;
    }

    public char getBuildingLetter() {
        return buildingLetter;
    }

    //“Address: <city> <zipCode>, <street> <buildingNumber><Literal>-<apartmentNumber>

    @Override
    public String toString() {
//        final StringBuilder sb = new StringBuilder("Address{");
//        sb.append("cityName='").append(cityName).append('\'');
//        sb.append(", zipCode=").append(zipCode);
//        sb.append(", streetName='").append(streetName).append('\'');
//        sb.append(", buildingNumber=").append(buildingNumber);
//        sb.append(", buildingLetter=").append(buildingLetter);
//        sb.append(", apartmentNumber=").append(apartmentNumber);
//        sb.append('}');
//        return sb.toString();
        Address address = new Address();
        String sf = String.format("Adress = { cityName = %s , zipCode = %d , streetName = %s, buildingNumber = %d, " +
                "buildingLetter = %s, apartmentNumber = %d }", cityName, zipCode, streetName, buildingNumber, buildingLetter, apartmentNumber);
        return sf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return zipCode == address.zipCode &&
                buildingNumber == address.buildingNumber &&
                apartmentNumber == address.apartmentNumber &&
                buildingLetter == address.buildingLetter &&
                cityName.equals(address.cityName) &&
                streetName.equals(address.streetName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityName) ^ Objects.hash(zipCode) ^ Objects.hash(streetName) ^
                Objects.hash(buildingNumber) ^ Objects.hash(apartmentNumber) ^ Objects.hash(buildingLetter);
    }
}

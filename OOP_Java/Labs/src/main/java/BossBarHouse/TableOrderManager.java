package BossBarHouse;

import java.time.LocalDate;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

public class TableOrderManager implements OrdersManager, List<Order> {

    /*Создайте публичный класс TableOrderManager, управляющий заказами.
    Этот класс основан массиве заказов. Номер элемента массива соответствует номеру столика в
    нашем заведении. Если элемент содержит null – значит столик еще свободен или уже свободен.*/

    Order[] orders;

    /*принимающий один параметр – число столиков, инициализирующий массив
    соответствующим числом элементов.*/
    public TableOrderManager(int numberTables) {
        if (numberTables < 0) {
            throw new NegativeSizeException("Задаваемый размер массива не может быть отрицательным числом.");
        }
        orders = new Order[numberTables];
    }


    /*добавления заказа столику. В качестве параметров принимает номер столика и ссылку на
        заказ.*/
    public void add(int numberOfTable, Order order) {
        if (Objects.nonNull(orders[numberOfTable])) { //todo я тебя не-на-ви-жу! Objects.nonNull есть же! DONE
            try {
                throw new AlreadyAddedException("Столик занят.");
            } catch (AlreadyAddedException e) {
                e.printStackTrace();
            }
        }
        orders[numberOfTable] = order;
    }


    //получения заказа столика по его номеру. В качестве параметра принимает номер столика.
    public Order getOrder(int numberOfTable) {
        return orders[numberOfTable];
    }

    //метод добавления блюда к заказу. В качестве параметра принимает номер столика и блюдо.
    public void addItem(int numberOfTable, MenuItem menuItem) {
        orders[numberOfTable].add(menuItem);
    }

    /*освобождения столика (по сути устанавливает значение соответствующего элемента
    массива в null). В качестве параметра принимает номер столика.*/
    public Order remove(int numberOfTable) {
        Order order = orders[numberOfTable];
        orders[numberOfTable] = null;
        return order;
    }

    //возвращающий номер первого найденного свободного столика.
    public int getFreeTableNumber() throws NoFreeTableException {
        for (int i = 0; i < orders.length; i++) {
            if (orders[i] == null) {
                return i;
            }
        }
        throw new NoFreeTableException("Свободных столиков нет.");
    }

    // возвращающий массив номеров свободных столиков.
    public int[] getFreeTableNumbers() {
        return tableNumbers(isFree);
    }

    // возвращающий массив номеров занятых столиков.
    public int[] getOccupiedTableNumbers() {
        return tableNumbers(isOccupied);

    }

    private int[] tableNumbers(Predicate<Order> predicate) {
        int tableCount = 0;
        int[] tableNumbers = new int[this.orders.length];
        for (int i = 0; i < orders.length; i++) {
            if (predicate.test(orders[i])) {
                tableNumbers[tableCount++] = i;
            }
        }
        int[] result = new int[tableCount];
        System.arraycopy(tableNumbers, 0, result, 0, tableCount);
        return result;
    }


    // возвращающий массив имеющихся на данный момент заказов.
    public Order[] getOrders() {
        Order[] availableOrders;
        int[] tableNumbers = getOccupiedTableNumbers();
        availableOrders = new Order[tableNumbers.length];
        for (int i = 0; i < availableOrders.length; i++) {
            availableOrders[i] = this.orders[tableNumbers[i]];
        }
        return availableOrders;
    }

    // возвращающий суммарную стоимость имеющихся на данный момент заказов.
    public int ordersCostSummary() {
        int costSum = 0;

        for (int i = 0; i < orders.length; i++) {
            if (!Objects.isNull(orders[i])) {
                costSum += orders[i].costTotal();
            }


        }
        return costSum;
    }

    // возвращающий общее число заказов.
    public int ordersQuantity() {
        return getOrders().length;
    }

    @Override
    public int ordersQuantity(LocalDate localDate) {
        int ordersQuantity = 0;
        for (Order order : getOrders()) {
            if ((order.getLocalDateTime().toLocalDate().equals(localDate))) {
                ordersQuantity++;
            }
        }
        return ordersQuantity;
    }

    @Override
    public Order[] getOrders(LocalDate localDate) {
        return checkObjectEquals(localDateOrderBiPredicate, localDate);
    }

    @Override
    public Order[] getOrders(Customer customer) {
        return checkObjectEquals(customerOrderBiPredicate, customer);
    }

    private <T> Order[] checkObjectEquals(BiPredicate<T, Order> biPredicate, T t) {
        Order[] result = new Order[orders.length];
        int j = 0;
        for (Order order : getOrders()) {
            if (biPredicate.test(t, order)) {
                result[j] = order;
                j++;
            }
        }
        return Arrays.copyOf(result, j);
    }

    private BiPredicate<LocalDate, Order> localDateOrderBiPredicate = (LocalDate localDate, Order order) -> order.getLocalDateTime().toLocalDate().equals(localDate);
    private BiPredicate<Customer, Order> customerOrderBiPredicate = (Customer customer, Order order) -> order.getCustomer().equals(customer);


    //  возвращающий общее среди всех занятых столиков количество заказанных порций
    //        заданного блюда по его имени. Принимает имя блюда в качестве параметра.
    //
    public int itemQuantity(String dishName) {
        return itemQuantity((p) -> p.itemQuantity(dishName));
    }

    // возвращающий общее среди всех занятых столиков количество заданных позиций.
    //Принимает в качестве параметра объект по ссылке типа MenuItem.

    public int itemQuantity(MenuItem menuItem) {
        return itemQuantity((p) -> p.itemQuantity(menuItem));
    }

    private int itemQuantity(Function<Order, Integer> function) {
        int count = 0;
        Order order;
        for (int i = 0; i < orders.length; i++) {
            order = orders[i];
            if (!Objects.isNull(orders[i])) {
                count += function.apply(order);
            }
        }
        return count;
    }

    //удаляющий заданный заказ. Принимает заказ в качестве параметра. Возвращает номер
    //столика удаленного заказа (или -1, если ничего не удалил).
    public int removeOrder(Order order) {
        for (int i = 0; i < orders.length; i++) {
            if (orders[i].equals(order)) {
                orders[i] = null;
                return i + 1;
            }
        }
        return -1;
    }

    // удаляющий все совпадения с заданным заказом. Принимает заказ в качестве
    //параметра. Возвращает число удаленных заказов (или -1, если ничего не удалил).
    public int removeAll(Order order) {
        int count = 0;
        for (int i = 0; i < orders.length; i++) {
            if (orders[i].equals(order)) {
                orders[i] = null;
                count++;
            }
        }
        return count;
    }


    @Override
    public int size() {
        return orders.length;
    }

    @Override
    public boolean isEmpty() {
        return orders.length == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (Order order : orders) {
            if (order.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<Order> iterator() {
        return new Iterator<Order>() {
            int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size();
            }

            @Override
            public Order next() {
                if (hasNext()) {
                    return orders[currentIndex++];
                }
                throw new NoSuchElementException("Элемента не существует");
            }

        };
    }

    @Override
    public Object[] toArray() {
        return getOrders();
    }

    @Override
    public <T> T[] toArray(T[] a) {

        if (a.length < orders.length) {
            return (T[]) Arrays.copyOf(orders, orders.length - 1, a.getClass());
        }
        System.arraycopy(orders, 0, a, 0, orders.length);
        if (a.length > orders.length) {
            a[orders.length - 1] = null;
        }

        return a;
    }

    @Override
    public boolean add(Order order) {
        add(orders.length - 1, order);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < orders.length; i++) {
            if (o.equals(orders[i])) {
                orders[i] = null;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object order : c) {
            if (!contains(order)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Order> c) {
        for (Order order : c) {
            add(order);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Order> c) {
        for (int i = index; i <c.size() ; i++) {
            add(orders[i]);
        }
        return true;
    }

    @Override
    public Order get(int index) {
        if (index >= 0 && index < size()) {
            return orders[index];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public Order set(int index, Order element) {
        Order order = orders[index];
        orders[index] = element;
        return order;
    }

    private BiPredicate<Collection<?>, Order> containsIn = Collection::contains;
    private BiPredicate<Collection<?>, Order> notContainsIn = (c, o) -> !c.contains(o);

    private <t> int removeAll(BiPredicate<t, Order> biPredicate, t collection) {
        int removedCount = 0;
        for (int i = 0; i < orders.length; i++) {
            if (biPredicate.test(collection, orders[i])) {
                remove(i);
                removedCount++;
            }
        }
        if (removedCount > 0) {
            return removedCount;
        }
        return -1;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return removeAll(containsIn, c) > 0;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return removeAll(notContainsIn, c) > 0;
    }

    @Override
    public void clear() {
        this.forEach(this::remove);
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < orders.length; i++) {
            if(orders[i].equals(o)){
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = orders.length - 1; i >= 0; i--) {
            if (orders[i].equals(o)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public ListIterator<Order> listIterator() {
        return Arrays.asList(orders).listIterator();
    }

    @Override
    public ListIterator<Order> listIterator(int index) {
        ListIterator<Order> listIterator = Arrays.asList(orders).listIterator();
        for (int i = 0; i <index ; i++) {
            listIterator.next();
        }
        return listIterator;
    }

    @Override
    public List<Order> subList(int fromIndex, int toIndex) {
        Order[] subList = new Order[toIndex-fromIndex];
        for (int i = fromIndex; i < toIndex ; i++) {
            if(Objects.nonNull(orders[i])){
                subList[i] = orders[i];
            }
        }
        return Arrays.asList(subList);
    }

}

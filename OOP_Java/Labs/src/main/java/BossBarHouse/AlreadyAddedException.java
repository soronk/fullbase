package BossBarHouse;

public class AlreadyAddedException extends Exception {
    public AlreadyAddedException(String exceptionMessage) {
        super(exceptionMessage);
    }
}

package BossBarHouse;

public class NoFreeTableException extends Exception {
    public NoFreeTableException(String message) {
        super(message);
    }
}

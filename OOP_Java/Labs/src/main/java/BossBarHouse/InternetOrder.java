package BossBarHouse;


import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;

public class InternetOrder implements Order {

    private int size = 0;
    private ListNode head;
    private ListNode tail;
    private Customer customer;
    private LocalDateTime localDateTime;

    public InternetOrder() {
        this.customer = new Customer();
        this.localDateTime = LocalDateTime.now();
    }

    public InternetOrder(MenuItem[] menuItems, Customer customer) {
        this.customer = customer;
        this.localDateTime = LocalDateTime.now();
        for (MenuItem menuItem : menuItems) {
            add(menuItem);
        }
    }


    //  добавляющий позицию в заказ (принимает ссылку на объект типа MenuItem). Пока
    //этот метод возвращает истину после выполнения операции добавления элемента.
    public boolean add(MenuItem menuItem) {
        //todo где UnlawfulActionException?
        ListNode element = new ListNode();
        element.value = menuItem;
        if (tail == null) {
            head = element;
            tail = element;
        } else {
            tail.next = element;
            tail = element;
        }
        this.size++;
        return true;
    }


    // удаляющий позицию из заказа по его названию (принимает название в качестве
    //параметра). Если позиций с заданным названием несколько, удаляется первое найденное.
    //Возвращает логическое значение (true, если элемент был удален).

    private boolean remove(Predicate<MenuItem> predicate) {
        if (head == null) {
            return false;
        }

        if (head == tail && head.value.getName().equals(tail.value.getName())) {
            head = null;
            tail = null;
            this.size--;
            return true;
        }

        if (predicate.test(head.value)) {
            head = head.next;
            this.size--;
            return true;
        }
        ListNode t = head;
        while (t.next != null) {
            if (predicate.test(t.next.value)) {
                if (predicate.test(tail.value)) {
                    tail = t;
                }
                t.next = t.next.next;
                this.size--;
                return true;
            }
            t = t.next;
        }

        return false;
    }


    //todo дублирование кода в 2-х следующих методах. Убираем через приватный метод, принимающий предикат в качестве параметра. Все никак... DONE
    public boolean remove(String name) {
        return remove((p) -> p.getName().equals(name));
    }


    //удаляющий позицию из заказа. Принимает в качестве параметра объект по ссылке
    //MenuItem (сравнение осуществляется с помощью метода equals()). Если эквивалентных
    //позиций несколько, удаляется первое найденное. Возвращает логическое значение (true, если
    //элемент был удален).
    public boolean remove(Object menuItem) {
        return remove((p) -> p.equals(menuItem));
    }

    //удаляющий все позиции с заданным именем (принимает название в качестве
    //параметра). Возвращает число удаленных элементов.

    private int removeAll(Predicate<MenuItem> predicate) {
        int count = 0;
        if (head == null) {
            return count;
        }

        if (head == tail && head.value.getName().equals(tail.value.getName())) {
            head = null;
            tail = null;
            this.size--;
            count++;
        }

        if (predicate.test(head.value)) {
            head = head.next;
            this.size--;
            count++;
        }
        ListNode t = head;
        while (t.next != null) {
            if (predicate.test(t.next.value)) {
                if (predicate.test(tail.value)) {
                    tail = t;
                }
                t.next = t.next.next;
                this.size--;
                count++;
            }
            t = t.next;
        }

        return count;
    }

    //todo дублирование кода в 2-х следующих методах. Убираем через приватный метод, принимающий предикат в качестве параметра. Все никак... DONE
    public int removeAll(String name) {
        return removeAll((p) -> p.getName().equals(name));
    }


    //удаляющий все позиции эквивалентные передаваемой в качестве параметра (по
    //ссылке типа MenuItem). Возвращает число удаленных элементов
    public int removeAll(MenuItem menuItem) {
        return removeAll((p) -> p.equals(menuItem));
    }

    // возвращающий общее число позиций (повторяющиеся тоже считаются) в заказе.
    public int itemQuantity() {
        return this.size;
    }


    //возвращающий массив позиций заказа (значений null в массиве быть не должно).
    public MenuItem[] getItems() {
        MenuItem[] menuItems = new MenuItem[this.size];
        ListNode t = head;
        for (int i = 0; i < this.size; i++) {
            menuItems[i] = t.value;
            t = t.next;
        }
        return menuItems;
    }

    //возвращающий общую стоимость заказа.
    public double costTotal() {
        ListNode t = head;
        double totalCost = 0;

        for (int i = 0; i < this.size; i++) {
            totalCost += t.value.getCost();
            t = t.next;
        }
        return totalCost;
    }

    //возвращающий число позиций в заказе (принимает название в качестве параметра)

    private int itemQuantity(Predicate<MenuItem> predicate) {
        int count = 0;
        ListNode currentNode = head;
        for (int i = 0; i < this.size; i++) {
            if (predicate.test(currentNode.value)) {
                count++;
                currentNode = currentNode.next;
            }
        }
        return count;
    }

    public int itemQuantity(String itemName) {
        return itemQuantity((p) -> p.getName().equals(itemName));
    }

    //возвращающий число заказанных блюд или напитков (принимает объект по ссылке
    //типа MenuItem в качестве параметра).

    public int itemQuantity(MenuItem menuItem) {
        return itemQuantity((p) -> p.equals(menuItem));
    }

    //возвращающий массив названий заказанных блюд и напитков (без повторов).
    //todo на будущее можно будет сделать default метод в интерфейсе, когда сделаешь итератор
    public String[] itemsNames() {
        ListNode t = head;
        String[] menuItemsNames = new String[this.size];
        menuItemsNames[0] = t.value.getName();
        String[] resultMenuItemsNames;
        int menuItemsCount = 1;
        for (int i = 0; i < this.size; i++) {
            if (!contains(menuItemsNames, menuItemsCount, t.value.getName())) {
                menuItemsNames[i] = t.value.getName();
                menuItemsCount++;
                t = t.next;
            }
        }

        resultMenuItemsNames = new String[menuItemsCount];
        System.arraycopy(menuItemsNames, 0, resultMenuItemsNames, 0, menuItemsCount);
        return resultMenuItemsNames;
    }

    private boolean contains(String[] menuItemsNames, int menuItemsCount, String name) {
        for (int i = 0; i < menuItemsCount; i++) {
            if (menuItemsNames[i].equals(name)) {
                return true;
            }
        }
        return false;
    }


    //возвращающий массив позиций, отсортированный по убыванию цены.
    public MenuItem[] sortedItemsByCostDesc() {
        MenuItem temp;
        MenuItem[] sortedMenuItemsArray = getItems();

        mergeSortMenuItems(sortedMenuItemsArray);

        for (int i = 0; i < this.size; i++) {
            temp = sortedMenuItemsArray[i];
            sortedMenuItemsArray[i] = sortedMenuItemsArray[sortedMenuItemsArray.length - i - 1];
            sortedMenuItemsArray[sortedMenuItemsArray.length - i - 1] = temp;
        }
        Arrays.sort(sortedMenuItemsArray, MenuItem::compareTo);

        return sortedMenuItemsArray;
    }


    private static void mergeSortMenuItems(MenuItem[] menuItems) {
        if (menuItems.length > 1) {
            int leftPartLength = menuItems.length / 2;
            int rightPartLength = menuItems.length - leftPartLength;

            MenuItem[] leftArray = new MenuItem[leftPartLength];
            MenuItem[] rightArray = new MenuItem[rightPartLength];

            System.arraycopy(menuItems, 0, leftArray, 0, leftPartLength);
            System.arraycopy(menuItems, leftPartLength, rightArray, 0, rightPartLength);

            mergeSortMenuItems(leftArray);
            mergeSortMenuItems(rightArray);

            int leftArrayIndex = 0;
            int rightArrayIndex = 0;
            int targetIndex = 0;

            while (targetIndex < menuItems.length) {
                if (leftArrayIndex >= leftArray.length) {
                    menuItems[targetIndex] = rightArray[rightArrayIndex];
                    rightArrayIndex++;
                } else if (rightArrayIndex >= rightArray.length) {
                    menuItems[targetIndex] = leftArray[leftArrayIndex];
                    leftArrayIndex++;
                } else if (leftArray[leftArrayIndex].getCost() >= rightArray[rightArrayIndex].getCost()) {
                    menuItems[targetIndex] = leftArray[leftArrayIndex];
                    leftArrayIndex++;
                } else {
                    menuItems[targetIndex] = rightArray[rightArrayIndex];
                    rightArrayIndex++;
                }
                targetIndex++;
            }
        }
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    @Override
    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }


    @Override
    public String toString() {
        MenuItem[] menuItems = getItems();
        final StringBuffer sb = new StringBuffer("TableOrder:{\n");
        sb.append("capacity=").append(size);
        if (menuItems == null) {
            sb.append("menuItems=").append(" null\n");
        }
        //todo циклом по нодам, не надо создавать массив DONE
        else for (ListNode listNode = head; listNode.next != null; listNode = listNode.next) {
            sb.append('<').append(listNode.value).append(">\n");
        }
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(o == null || !(o instanceof InternetOrder))return false;

        //return itemQuantity() == that.itemQuantity() && customer.equals(that.size /*ШТА?*/) &&
        //       Arrays.equals(sortedItemsByCostDesc(), that.sortedItemsByCostDesc()); //todo капец. СОздаем и сортируем массивы чтоб сравнить... DONE
        /*todo циклом по нодам, и для каждого своего node.value делаем DONE
        if(itemQuantity(node.value) != that.itemQuantity(node.value))
            return false;
        */
        InternetOrder that = (InternetOrder) o;
        ListNode equalNode = that.head;

        for (ListNode listNode = head; listNode.next != null; listNode = listNode.next) {
            if (Objects.nonNull(listNode.value) && Objects.nonNull(equalNode.value) && (listNode.value instanceof MenuItem )) {
                return listNode.value.equals(equalNode.value);
            }
            equalNode = equalNode.next;
        }

        boolean flagCustomer = true;
        if (Objects.nonNull(customer) && Objects.nonNull(that.customer)) {
            flagCustomer = customer.equals(that.customer);
        }
        return flagCustomer && size == that.size && localDateTime.equals(that.localDateTime);
    }

    @Override
    public int hashCode() {
        //MenuItem[] menuItems = getItems();
        int hcObj = Objects.hashCode(head);
        int hcPos = Objects.hashCode(0);
        int index = 1;
//        for (int i = 1; i < menuItems.length; i++) {
//            //todo циклом по нодам, не надо создавать массив DONE
//            hcObj ^= Objects.hashCode(menuItems[i]);
//            hcPos ^= i;
//        }
        for (ListNode listNode = head.next; listNode.next != null; listNode = listNode.next) {
            hcObj ^= Objects.hashCode(listNode.value);
            hcPos ^= index;
            index++;
        }
        return hcObj ^ hcPos;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        MenuItem item = (MenuItem) o;
        for (MenuItem menuItem : this) {
            if (menuItem.equals(item)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<MenuItem> iterator() {
        return new Iterator<MenuItem>() {
            private ListNode current = head;

            @Override
            public boolean hasNext() {
                return Objects.nonNull(current.next);
            }

            @Override
            public MenuItem next() {
                if (hasNext()) {
                    current = current.next;
                    return head.value;
                }
                throw new NoSuchElementException("Элементов не обнаружено.");
            }
        };
    }

    @Override
    public Object[] toArray() {
        return getItems();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < size) {
            return (T[]) Arrays.copyOf(getItems(), size - 1, a.getClass());
        }
        System.arraycopy(getItems(), 0, a, 0, size);
        if (a.length > size) {
            a[size] = null;
        }
        return a;
    }

    @Override
    public MenuItem get(int index) {
        if (index >= 0 && index < size()) {
            return getItems()[index];
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public MenuItem set(int index, MenuItem element) {
        getItems()[index] = element;
        return getItems()[index];
    }

    @Override
    public void add(int index, MenuItem element) {
        getItems()[index] = element;
    }

    @Override
    public MenuItem remove(int index) {
        MenuItem menuItem = get(index);
        remove(menuItem);
        return menuItem;
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        MenuItem menuItem = (MenuItem) o;
        for (ListNode node = head; node != null; node = node.next) {
            if (menuItem.equals(node.value)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int i = 0;
        int index = -1;
        MenuItem menuItem = (MenuItem) o;
        for (ListNode node = head; node != null; node = node.next) {
            if (menuItem.equals(node.value)) {
                index = i;
            }
            i++;
        }
        return index;
    }

    @Override
    public ListIterator<MenuItem> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<MenuItem> listIterator(int index) {
        return new ListIterator<MenuItem>() {
            ListNode current = head;
            int currentIndex = index;
            MenuItem lastReturned = null;

            @Override
            public boolean hasNext() {
                return Objects.nonNull(current.next);
            }

            @Override
            public MenuItem next() {
                if (hasNext()) {
                    current = current.next;
                    lastReturned = current.value;
                    return current.value;
                }
                throw new NoSuchElementException("Элемент не найден.");
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public MenuItem previous() {
                return null;
            }

            @Override
            public int nextIndex() {
                if (hasNext()) {
                    return currentIndex + 1;
                }
                return currentIndex;
            }

            @Override
            public int previousIndex() {
                if (hasPrevious()) {
                    return currentIndex - 1;
                }
                return currentIndex;
            }

            @Override
            public void remove() {
                if (Objects.isNull(lastReturned)) {
                    throw new IllegalStateException("Вы не можете удалить элемент.");
                }
                lastReturned = null;
                InternetOrder.this.remove(currentIndex);
            }

            @Override
            public void set(MenuItem item) {
                if (Objects.isNull(lastReturned)) {
                    throw new IllegalStateException("Вы не можете изменить значение этого элемента");
                }
                InternetOrder.this.set(currentIndex, item);
            }

            @Override
            public void add(MenuItem item) {
                lastReturned = null;
                InternetOrder.this.add(currentIndex, item);
            }
        };
    }

    @Override
    public List<MenuItem> subList(int fromIndex, int toIndex) {
        List<MenuItem> subList = new InternetOrder();
        ListNode current = head;
        for (int i = 0; i < fromIndex; i++) {
            current = current.next;
        }
        while (fromIndex <= toIndex) {
            subList.add(current.value);
            current = current.next;
            fromIndex++;
        }
        return subList;
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            MenuItem menuItem = (MenuItem) o;
            if (!contains(menuItem)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends MenuItem> c) {
        addAll(0, c);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends MenuItem> c) {
        ListNode current = head;
        int nodeIndex = 0;
        for (MenuItem menuItem : c) {
            if (c.contains(menuItem)) {
                try {
                    throw new AlreadyAddedException("Ошибка");
                } catch (AlreadyAddedException e) {
                    e.printStackTrace();
                }
            }
            if (nodeIndex >= index) {
                current.value = menuItem;
                current = current.next;
                this.size++;
            }
            nodeIndex++;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (ListNode listNode = head; listNode != null; listNode = listNode.next) {
            if (c.contains(listNode)) {
                remove(listNode.value);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (ListNode listNode = head; listNode != null; listNode = listNode.next) {
            if (!c.contains(listNode)) {
                remove(listNode.value);
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        this.forEach(this::remove);
    }

}

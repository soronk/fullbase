package BossBarHouse;

import java.util.Objects;

public abstract class MenuItem implements Comparable<MenuItem> {

    /*Создайте публичный класс Dish – блюда некоторого заведения общественного питания.
    Класс характеризуется стоимостью, названием и строкой-описанием блюда.*/
    private double cost;
    private String name;
    private String description;
    protected static final double DEFAULT_COST = 0;

    @Override
    public int compareTo(MenuItem o) {
        //todo Double.compare(d1, d2) не? разрешено же вроде уже... DONE
        return Double.compare(o.getCost(), this.cost);
//        double compareCost = o.getCost();
//        if (compareCost > this.cost) {
//            return -1;
//        } else if (compareCost < this.cost) {
//            return 1;
//        } else
//            return 0;
    }

    /*принимающий два параметра – название и описание блюда. Стоимость при этом
    инициализируется значением 0;*/
    //todo да сколько можно? КОНСТРУКТОР С меньшим числом параметров вызывает конструктор с большим числом параметром, если речь идет о дефолтных параметрах. ЭТО ПОЗВОЛЯЕТ УБРАТЬ ДУБЛИРОВАНИЕ КОДА ИНИЦИАЛИЗАЦИИ!!!!!!!!!!! DONE
    protected MenuItem(String name, String description) {
        this(DEFAULT_COST, name, description);
    }

    //принимающий три параметра – стоимость, название и описание блюда.
    protected MenuItem(double cost, String name, String description) {
        if (cost < 0) {
            throw new IllegalArgumentException("Стоимость не может быть отрицательным числом");
        }
        this.cost = cost;
        this.name = name;
        this.description = description;
    }

    //возвращающий стоимость.
    public double getCost() {
        return cost;
    }


    //возвращающий название.
    public String getName() {
        return name;
    }


    //возвращающий описание.
    public String getDescription() {
        return description;
    }


    @Override
    public String toString() {

        String sf = String.format("MenuItem: {name = %s, cost = %d}", name, cost);
        return sf;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuItem menuItem = (MenuItem) o;
        return Double.compare(menuItem.cost, cost) == 0 &&
                name.equals(menuItem.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cost) ^ Objects.hash(name);
    }
}

package BossBarHouse;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Predicate;

public interface OrdersManager extends Collection<Order> {

    public int itemQuantity(String itemName);
    public int itemQuantity(MenuItem item);
    public Order[] getOrders();
    public int ordersCostSummary();
    public int ordersQuantity();
    public int ordersQuantity(LocalDate localDate);
    public Order[] getOrders(LocalDate localDate);
    public Order[] getOrders(Customer customer);


    Predicate<Order> isFree = Objects::isNull;
    Predicate<Order> isOccupied = Objects::nonNull;

}

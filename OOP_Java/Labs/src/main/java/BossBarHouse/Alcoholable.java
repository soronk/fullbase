package BossBarHouse;

public interface Alcoholable {

    public boolean isAlcoholDrink();
    public double getAlcoholVol();

}

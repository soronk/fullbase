package BossBarHouse;

import java.util.Objects;

public class Dish extends MenuItem {

    public Dish(String name, String description) {
        super(name, description);
    }

    public Dish(double cost, String name, String description) {
        super(cost, name, description);
    }

    @Override
    public String toString() {
//
//        final StringBuilder sb = getString();
//        sb.insert(0,"Dish{");
//        sb.append(", description='").append(getDescription()).append('\'');
//        sb.append('}');
//        return sb.toString();

        String sf = String.format("Dish: {description = %s, name = %s, cost = %d}", getDescription(), getName(),getCost());
        return sf;
    }

    @Override
    public int hashCode() {
        return super.hashCode() ^ Objects.hash(getDescription());
    }


}

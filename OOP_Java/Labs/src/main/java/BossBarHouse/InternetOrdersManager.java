package BossBarHouse;


import java.time.LocalDate;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Function;

public class InternetOrdersManager implements OrdersManager, Deque<Order> {

    private QueueNode head;
    private QueueNode tail;
    private int size;

    public InternetOrdersManager() {
        head = null;
        tail = null;
        this.size = 0;
    }

    public InternetOrdersManager(Order[] orders) throws AlreadyAddedException {
        for (Order order : orders) {
            add(order);
        }
    }


    // добавления заказа в конец очереди. Пока возвращает истину
    public boolean add(Order order) {
        QueueNode queueNode = new QueueNode();
        queueNode.value = order;

        //todo циклом пробегаемся по нодам и для каждого сравниваем customer и время, а ты написал херню DONE
        checkAlreadyAdded(order);

        if (this.size == 0) {
            head = queueNode;
        } else {
            tail.next = queueNode;
            queueNode.prev = tail;
        }
        tail = queueNode;
        this.size++;
        return true;
    }


    // получения первого в очереди заказа без его удаления из очереди.
    public Order order() {
        if (size == 0) {
            return null;
        }
        return head.value;
    }

    // получения первого в очереди заказа с удалением его из очереди.
    public Order remove() {

        if (this.size == 0) {
            return null;
        }
        Order order = null;

        if (head != null) {
            order = head.value;
            head.next = head;
        }
        if (head == null) {
            tail = null;
        }
        this.size--;

        return order;
    }


    // возвращающий общее число заказов в очереди.
    @Override
    public int ordersQuantity() {
        return this.size;
    }

    @Override
    public int ordersQuantity(LocalDate localDate) {
        QueueNode queueNode = head;
        int ordersQuantity = 0;
        for (int i = 0; i < this.size; i++) {
            InternetOrder internetOrder = (InternetOrder) queueNode.value;
            if ((internetOrder.getLocalDateTime().toLocalDate().equals(localDate))) {
                ordersQuantity++;
            }
            queueNode = queueNode.next;
        }
        return ordersQuantity;
    }

    @Override
    public Order[] getOrders(LocalDate localDate) {
        return checkObjectEqualsOrders(localDateInternetOrderBiPredicate, localDate);
    }

    @Override
    public Order[] getOrders(Customer customer) {
        return checkObjectEqualsOrders(customerInternetOrderBiPredicate, customer);
    }

    private <T> Order[] checkObjectEqualsOrders(BiPredicate<T, InternetOrder> biPredicate, T object) {
        Order[] result = new Order[size];
        QueueNode queueNode = head;
        int j = 0;
        for (int i = 0; i < size; i++) {
            InternetOrder internetOrder = (InternetOrder) queueNode.value;
            if (biPredicate.test(object, internetOrder)) {
                result[j] = queueNode.value;
                j++;
            }
            queueNode = queueNode.next;
        }
        return Arrays.copyOf(result, j);
    }

    private BiPredicate<Customer, InternetOrder> customerInternetOrderBiPredicate = (customer, internetOrder) -> internetOrder.getCustomer().equals(customer);
    private BiPredicate<LocalDate, InternetOrder> localDateInternetOrderBiPredicate = (localDate, internetOrder) -> internetOrder.getLocalDateTime().toLocalDate().equals(localDate);

    //возвращающий массив имеющихся на данный момент в очереди заказов
    @Override
    public Order[] getOrders() {
        Order[] orders = new Order[this.size];
        QueueNode queueNode = head;
        for (int i = 0; i < this.size; i++) {
            orders[i] = queueNode.value;
            queueNode = queueNode.next;
        }
        return orders;
    }

    // возвращающий общее среди всех занятых столиков количество заказанных порций
    //заданного блюда по его имени. Принимает имя блюда в качестве параметра.
    @Override
    public int itemQuantity(String itemName) {
        return itemQuantity((queueNode -> queueNode.value.itemQuantity(itemName)));
    }

    // возвращающий общее среди всех занятых столиков количество заданных позиций.
    //Принимает в качестве параметра объект по ссылке типа MenuItem.
    @Override
    public int itemQuantity(MenuItem item) {
        return itemQuantity((queueNode -> queueNode.value.itemQuantity(item)));
    }

    public int itemQuantity(Function<QueueNode, Integer> function) {
        int count = 0;
        QueueNode queueNode = head;
        while (queueNode != null) {
            count += function.apply(queueNode);
            queueNode = queueNode.next;
        }
        return count;
    }

    // возвращающий суммарную стоимость имеющихся на данный момент заказов
    @Override
    public int ordersCostSummary() {
        int ordersCostSummary = 0;
        QueueNode queueNode = head;
        while (queueNode != null) {
            ordersCostSummary += queueNode.value.costTotal();
            queueNode = queueNode.next;
        }
        return ordersCostSummary;
    }

    private void checkAlreadyAdded(Order order) {
        for (QueueNode qNode = head; qNode.next != null; qNode = qNode.next) {
            if (qNode.value.getCustomer().equals(order.getCustomer()) && order.getLocalDateTime().equals(order.getLocalDateTime())) {
                try {
                    throw new AlreadyAddedException("В списке заказов уже существует заказ с таким клиентом и временем заказа.");
                } catch (AlreadyAddedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void addFirst(Order order) {
        checkAlreadyAdded(order);
        QueueNode queueNode = new QueueNode();
        queueNode.value = order;

        if (Objects.isNull(head)) {
            head = queueNode;
            head.prev = head;
            tail = queueNode;
        } else {
            head.prev = queueNode;
            head = queueNode;
            queueNode.next = head.next;
        }
        this.size++;
    }


    @Override
    public void addLast(Order order) {
        add(order);
    }

    @Override
    public boolean offerFirst(Order order) {
        addFirst(order);
        return true;
    }

    @Override
    public boolean offerLast(Order order) {
        addLast(order);
        return true;
    }

    @Override
    public Order removeFirst() {
        return remove();
    }

    @Override
    public Order removeLast() {
        Order order = null;
        if (this.size == 0) {
            return null;
        }
        if (Objects.nonNull(tail)) {
            order = tail.value;
            tail = tail.prev;
        }
        if (Objects.isNull(head)) {
            tail = null;
        }
        this.size--;
        return order;
    }

    @Override
    public Order pollFirst() {
        if (Objects.isNull(head)) {
            return null;
        }
        return removeFirst();
    }

    @Override
    public Order pollLast() {
        if (Objects.isNull(tail)) {
            return null;
        }
        return removeLast();
    }

    @Override
    public Order getFirst() {
        if (Objects.isNull(head)) {
            throw new NoSuchElementException("Элемента не существует.");
        }
        return head.value;
    }

    @Override
    public Order getLast() {
        if (Objects.isNull(tail)) {
            throw new NoSuchElementException("Элемента не существует");
        }
        return tail.value;
    }

    @Override
    public Order peekFirst() {
        if (Objects.isNull(head)) {
            return null;
        }
        return head.value;
    }

    @Override
    public Order peekLast() {
        if (Objects.isNull(tail)) {
            return null;
        }
        return tail.value;
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return remove(o);
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        if (head.equals(tail) && o.equals(head.value)) {
            tail = null;
            this.size = 0;
            return true;
        }
        if (tail.value.equals(o)) {
            tail = tail.prev;
            tail.next = null;
            return true;
        }
        if (head.value.equals(o)) {
            head = head.next;
            return true;
        }
        for (QueueNode queueNode = tail; Objects.nonNull(queueNode); queueNode = queueNode.prev) {
            if (queueNode.value.equals(o)) {
                queueNode.next = queueNode.prev;
                queueNode.prev = queueNode.next;
                queueNode.value = null;
                return true;
            }
        }

        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return Objects.isNull(head);
    }

    @Override
    public boolean contains(Object o) {
        for (QueueNode queueNode = head; Objects.nonNull(queueNode); queueNode = queueNode.next) {
            if (queueNode.value.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<Order> iterator() {
        return new Iterator<Order>() {
            private QueueNode current = head;

            @Override
            public boolean hasNext() {
                return Objects.nonNull(current.next);
            }

            @Override
            public Order next() {
                if (hasNext()) {
                    current = current.next;
                    return head.value;
                }
                throw new NoSuchElementException("Элементов не обнаружено.");
            }
        };
    }

    @Override
    public Object[] toArray() {
        return getOrders();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < size) {
            return (T[]) Arrays.copyOf(getOrders(), size - 1, a.getClass());
        }
        System.arraycopy(getOrders(), 0, a, 0, size);
        if (a.length > size) {
            a[size] = null;
        }
        return a;
    }

    @Override
    public Order poll() {
        if (Objects.nonNull(head)) {
            return null;
        }
        return removeFirst();
    }

    @Override
    public Order element() {
        return getFirst();
    }

    @Override
    public Order peek() {
        if (Objects.isNull(head)) {
            return null;
        }
        return head.value;
    }

    @Override
    public void push(Order order) {
        addFirst(order);
    }

    @Override
    public Order pop() {
        return removeFirst();
    }

    //fixme исправить. Чет не то совсем
    @Override
    public Iterator<Order> descendingIterator() {
        return new ListIterator<Order>() {
            QueueNode currentNode = head;
            Order lastReturned = null;
            int currentIndex = size;

            @Override
            public boolean hasNext() {
                return Objects.nonNull(currentNode.next);
            }

            @Override
            public Order next() {
                if (hasNext()) {
                    currentNode = currentNode.next;
                    lastReturned = currentNode.value;
                }
                throw new NoSuchElementException("Элемента не существует");
            }

            @Override
            public boolean hasPrevious() {
                return Objects.nonNull(currentNode.prev);
            }

            @Override
            public Order previous() {
                if (hasPrevious()) {
                    currentNode = currentNode.prev;
                    lastReturned = currentNode.value;
                }
                throw new NoSuchElementException("Элемента не существует");
            }

            @Override
            public int nextIndex() {
                if (hasNext()) {
                    return currentIndex + 1;
                }
                return currentIndex;
            }

            @Override
            public int previousIndex() {
                if (hasPrevious()) {
                    return currentIndex - 1;
                }
                return currentIndex;
            }

            @Override
            public void remove() {
                if (Objects.isNull(lastReturned)) {
                    throw new IllegalStateException("Вы не можете удалить этот элемент");
                }
                lastReturned = null;
                InternetOrdersManager.this.remove(currentIndex);

            }

            @Override
            public void set(Order order) {

            }

            @Override
            public void add(Order order) {
                lastReturned = null;
                InternetOrdersManager.this.addLast(order);
            }
        };
    }


    @Override
    public boolean remove(Object o) {
        if (o.equals(head.value)) {
            head = head.next;
            head.prev = null;
            this.size--;
            return true;
        }
        if (o.equals(tail.value)) {
            tail = tail.prev;
            tail.next = null;
            this.size--;
            return true;
        }
        for (QueueNode queueNode = head; Objects.nonNull(queueNode); queueNode = queueNode.next) {
            if (o.equals(queueNode.value)) {
                queueNode.prev = queueNode.next;
                queueNode.next = queueNode.prev;
                queueNode.value = null;
                this.size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Order> c) {
        for (Order order : c) {
            add(order);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object o : c) {
            if (remove(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (QueueNode queueNode = head; Objects.nonNull(queueNode); queueNode = queueNode.next) {
            if (!c.contains(queueNode)) {
                remove(queueNode.value);
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        this.forEach(this::remove);
    }

    @Override
    public boolean offer(Order order) {
        return add(order);
    }

}

package BossBarHouse;

public class UnlawfulActionException extends RuntimeException {

    public UnlawfulActionException(String exceptionMessage) {
        super(exceptionMessage);
    }
}

package BossBarHouse;

import java.util.Objects;

public class Drink extends MenuItem implements Alcoholable {

    private DrinkTypeEnum drinkType;
    private double alcoholVol;

    private final static double DEFAULT_ALCOHOL_VOL = 0;

    protected Drink(double cost, String name, DrinkTypeEnum drinkType) {
        this(DEFAULT_COST, name, "", drinkType);
    }

    protected Drink(double cost, String name, String description, DrinkTypeEnum drinkType) {
        this(cost, name, description, drinkType, DEFAULT_ALCOHOL_VOL);
    }

    public Drink(double cost, String name, String description, DrinkTypeEnum drinkType, double alcoholVol) {
        super(cost, name, description);
        this.drinkType = drinkType;
        if (alcoholVol < 0 || alcoholVol > 100) {
            throw new IllegalArgumentException("Количество содержащегося алкоголя в напитке не может быть меньше нуля или больше 100");
        }
        this.alcoholVol = alcoholVol;
    }

    public DrinkTypeEnum getDrinkType() {
        return drinkType;
    }


    @Override
    public boolean isAlcoholDrink() {
        return false;
    }

    @Override
    public double getAlcoholVol() {
        return 0;
    }

    @Override
    public String toString() {
//        final StringBuilder sb = getString();
////        sb.insert(0, "Drink{ drinkType=");
////        sb.append("Alcohol:");
////        sb.append(", alcoholVol=").append(alcoholVol).append("%.\'");
////        sb.append(", description='").append(getDescription());
////        sb.append('}');
////        return sb.toString();
//
        String sf = String.format("Drink: {name = %s, cost = %d, Alcohol:, alcoholVol = %d, description = %s}",
                getName(), getCost(), alcoholVol, getDescription());
        return sf;

    }

    //public boolean equals(Object obj). 2 экземпляра класса Drink считаются равными
    //(equal), если у них одинаковый тип, стоимость, имена и процент спирта совпадают (описание
    //не сравнивается).
    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        Drink drink = (Drink) o;
        return Double.compare(drink.alcoholVol, alcoholVol) == 0 &&
                drinkType == drink.drinkType;

    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), drinkType, alcoholVol);
    }
}

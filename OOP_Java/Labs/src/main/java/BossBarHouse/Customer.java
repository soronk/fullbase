package BossBarHouse;

import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

public final class Customer {
    //В классе Customer замените поле int age на LocalDate birthDate. Конструктор также
    //должен работать с датой. Однако метод getAge() по-прежнему должен возвращать
    //целое число – возраст клиента.
    //todo в immutable классах поля тоже final должны быть DONE
    private final String firstName;
    private final String secondName;
    private final LocalDate birthDate;
    private final Address address;

    private final static String DEFAULT_FIRST_NAME = "";
    private final static String DEFAULT_SECOND_NAME = "";
    //private final static int DEFAULT_AGE = -1;

    public static final Customer MATURE_UNKNOWN_CUSTOMER = new Customer(LocalDate.of(1998, 01, 01));
    public static final Customer NOT_MATURE_UNKNOWN_CUSTOMER = new Customer(LocalDate.of(2005, 01, 01));

    public Customer() {
        this(DEFAULT_FIRST_NAME, DEFAULT_SECOND_NAME, LocalDate.now(), Address.EMPTY_ADRESS);
    }

    public Customer(LocalDate birthDate) {
        this(DEFAULT_FIRST_NAME, DEFAULT_SECOND_NAME, birthDate, Address.EMPTY_ADRESS);
    }

    public Customer(String firstName, String secondName, LocalDate birthDate, Address address) {
        this.firstName = firstName;
        this.secondName = secondName;

        if (birthDate.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Нельзя передавать дату из будущего");
        }
        this.birthDate = birthDate;

        this.address = address;
    }


    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getAge() {
        LocalDate nowLocalDate = LocalDate.now();
        Period period = Period.between(birthDate, nowLocalDate);
        return period.getYears();
    }

    public Address getAddress() {
        return address;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Customer{");
        sb.append("secondName='").append(secondName).append('\'');
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", birthDate=").append(birthDate);
        if (!address.equals(new Address().EMPTY_ADRESS)) {
            sb.append(", address=").append(address);
        }
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return birthDate == customer.birthDate &&
                firstName.equals(customer.firstName) &&
                secondName.equals(customer.secondName) &&
                address.equals(customer.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName) ^ Objects.hash(secondName) ^
                Objects.hash(birthDate) ^ Objects.hash(address);
    }
}

package BossBarHouse;

public class NegativeSizeException extends NegativeArraySizeException {

    public NegativeSizeException(String exceptionMessage){
        super(exceptionMessage);
    }

}

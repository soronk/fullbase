package BossBarHouse;


import java.time.LocalDateTime;
import java.util.*;

import static java.lang.System.arraycopy;


public class TableOrder implements Order {

    /*Создайте публичный класс Order – заказа.
    Этот класс характеризуется массивом блюд, числом заказанных блюд.*/
    private final int DEFAULT_CAPACITY = 16;
    private static final int DEFAULT_SIZE = 0;
    private static final int OVERTIME_FOR_SELL_ALCOHOL = 22;
    private static final int LEGAL_AGE_FOR_BUY_ALCOHOL = 21;
    private MenuItem[] menuItems;
    private int size;
    private Customer customer;
    private LocalDateTime localDateTime;

    /*не принимающий параметров, инициирующий массив из 16 элементов (сами элементы
    имеют значение null).*/
    public TableOrder() {
        this(DEFAULT_SIZE, new Customer());
    }

    /*принимающий целое число – емкость массива, инициирующий массив указанным числом
    элементов (сами элементы имеют значение null).*/
    public TableOrder(int size, Customer customer) {
        if (size < 0) {
            throw new NegativeSizeException("Задаваемый размер массива не может быть отрицательным числом.");
        } else if (size != 0) {
            menuItems = new MenuItem[size];
        } else {
            menuItems = new MenuItem[DEFAULT_CAPACITY];
        }
        this.customer = customer;
        this.localDateTime = LocalDateTime.now();
    }

    //принимающий массив блюд.
    public TableOrder(MenuItem[] menuItems, Customer customer) {
        if (menuItems.length != 0) {
            this.menuItems = new MenuItem[menuItems.length];
            arraycopy(menuItems, 0, this.menuItems, 0, menuItems.length);
            this.size = menuItems.length;
            this.customer = customer;
            this.localDateTime = LocalDateTime.now();
        }
    }


    /*добавляющий блюдо в заказ (принимает ссылку на экземпляр класса Dish). Пока этот метод
        возвращает истину после выполнения операции добавления элемента.*/
    public boolean add(MenuItem menuItem) {
        if (menuItem instanceof Drink) {
            Drink drink = (Drink) menuItem;
            if (drink.isAlcoholDrink() && (customer.getAge() < LEGAL_AGE_FOR_BUY_ALCOHOL || localDateTime.getHour() >= OVERTIME_FOR_SELL_ALCOHOL)) {
                throw new UnlawfulActionException("Клиент не достиг совершеннолетия или в данное время запрещена продажа алкоголя.");
            }
        }
        if (this.size == this.menuItems.length) {
            MenuItem[] newMenuItems = new MenuItem[this.menuItems.length * 2];
            arraycopy(this.menuItems, 0, newMenuItems, 0, this.menuItems.length);
            this.menuItems = newMenuItems;
        }
        this.menuItems[this.size] = menuItem;
        this.size++;
        return true;
    }


    /*удаляющий блюдо из заказа по его названию (принимает название блюда в качестве
    параметра). Если блюд с заданным названием несколько, удаляется первое найденное. Возвращает
    логическое значение (true, если элемент был удален).*/
    public boolean remove(String menuItemName) {
        for (int i = 0; i < this.size; i++) {
            if (menuItems[i].getName().equals(menuItemName)) {
                return shiftArray(i);
            }
        }
        return false;
    }

    /*удаляющий все блюда с заданным именем (принимает название блюда в качестве
    параметра). Возвращает число удаленных элементов.*/
    public int removeAll(String menuItemName) {
        int countRemovedMenuItems = 0;
        for (int i = 0; i < this.size; i++) {
            if (menuItems[i].getName().equals(menuItemName)) {
                menuItems[i] = null;
                countRemovedMenuItems++;
            }
        }
        menuItems = getItems();
        this.size -= countRemovedMenuItems;
        return countRemovedMenuItems;
    }

    //возвращающий общее число блюд (повторяющиеся блюда тоже считаются) в заказе.
    public int itemQuantity() {
        return this.size;
    }

    //возвращающий массив блюд (значений null в массиве быть не должно).
    public MenuItem[] getItems() {
        MenuItem[] allMenuItem = new MenuItem[this.size];
        arraycopy(menuItems, 0, allMenuItem, 0, allMenuItem.length);
        return allMenuItem;
    }

    //возвращающий общую стоимость заказа.
    public double costTotal() {
        double fullPrice = 0;
        for (int i = 0; i < this.size; i++) {
            fullPrice += menuItems[i].getCost();
        }
        return fullPrice;
    }

    //возвращающий число заказанных блюд (принимает название блюда в качестве параметра).
    public int itemQuantity(String menuItemName) {
        int countMenuItemNames = 0;
        for (int i = 0; i < menuItems.length; i++) {
            if (menuItems[i].getName().equals(menuItemName)) {
                countMenuItemNames++;
            }
        }
        return countMenuItemNames;
    }

    //возвращающий массив названий заказанных блюд (без повторов).
    public String[] itemsNames() {
        String[] menuItemsNames = new String[this.size];
        menuItemsNames[0] = this.menuItems[0].getName();
        String[] resultMenuItemsNames;
        int menuItemsCount = 1;
        for (int i = 1; i < this.size; i++) {
            if (!contains(menuItemsNames, menuItemsCount, this.menuItems[i].getName())) {
                menuItemsNames[i] = this.menuItems[i].getName();
                menuItemsCount++;
            }
        }

        resultMenuItemsNames = new String[menuItemsCount];
        arraycopy(menuItemsNames, 0, resultMenuItemsNames, 0, menuItemsCount);
        return resultMenuItemsNames;
    }

    private boolean contains(String[] menuItems, int menuItemsCount, String name) {
        for (int i = 0; i < menuItemsCount; i++) {
            if (menuItems[i].equals(name)) {
                return true;
            }
        }
        return false;
    }


    //возвращающий массив блюд, отсортированный по убыванию цены.
    public MenuItem[] sortedItemsByCostDesc() {

        MenuItem temp;
        MenuItem[] sortedMenuItemsArray = getItems();

        mergeSortMenuItems(sortedMenuItemsArray);

        //реверс массива, чтобы разместить элементы в обратном порядке
        //todo блять
        for (int i = 0; i < menuItems.length; i++) {
            temp = sortedMenuItemsArray[i];
            sortedMenuItemsArray[i] = sortedMenuItemsArray[sortedMenuItemsArray.length - i - 1];
            sortedMenuItemsArray[sortedMenuItemsArray.length - i - 1] = temp;
        }
        //todo если тебе надо в обратном порядке, просто написать вот так нельзя было?
        Arrays.sort(sortedMenuItemsArray, (i1, i2) -> -i1.compareTo(i2));

        return sortedMenuItemsArray;
    }


    private static void mergeSortMenuItems(MenuItem[] menuItems) {
        if (menuItems.length > 1) {
            int leftPartLength = menuItems.length / 2;
            int rightPartLength = menuItems.length - leftPartLength;

            MenuItem[] leftArray = new MenuItem[leftPartLength];
            MenuItem[] rightArray = new MenuItem[rightPartLength];

            arraycopy(menuItems, 0, leftArray, 0, leftPartLength);
            arraycopy(menuItems, leftPartLength, rightArray, 0, rightPartLength);

            mergeSortMenuItems(leftArray);
            mergeSortMenuItems(rightArray);

            int leftArrayIndex = 0;
            int rightArrayIndex = 0;
            int targetIndex = 0;

            while (targetIndex < menuItems.length) {
                if (leftArrayIndex >= leftArray.length) {
                    menuItems[targetIndex] = rightArray[rightArrayIndex];
                    rightArrayIndex++;
                } else if (rightArrayIndex >= rightArray.length) {
                    menuItems[targetIndex] = leftArray[leftArrayIndex];
                    leftArrayIndex++;
                } else if (leftArray[leftArrayIndex].getCost() >= rightArray[rightArrayIndex].getCost()) {
                    menuItems[targetIndex] = leftArray[leftArrayIndex];
                    leftArrayIndex++;
                } else {
                    menuItems[targetIndex] = rightArray[rightArrayIndex];
                    rightArrayIndex++;
                }

                targetIndex++;
            }
        }
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }


    @Override
    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    @Override
    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    //удаляющий позицию из заказа. Принимает в качестве параметра объект по ссылке
    //MenuItem (сравнение осуществляется с помощью метода equals()). Если эквивалентных
    //позиций несколько, удаляется первое найденное. Возвращает логическое значение (true, если
    //элемент был удален).
    public boolean remove(Object menuItem) {

        for (int i = 0; i < menuItems.length; i++) {
            if (menuItems[i].equals(menuItem)) {
                return shiftArray(i);
            }
        }
        return false;
    }


    private boolean shiftArray(int i) {
        arraycopy(this.menuItems, i + 1, this.menuItems, i, size - 1);
        this.menuItems[size] = null;
        this.size--;
        return true;
    }

    //удаляющий все позиции эквивалентные передаваемой в качестве параметра (по
    //ссылке типа MenuItem). Возвращает число удаленных элементов
    public int removeAll(MenuItem menuItem) {
        int countRemovedMenuItems = 0;

        for (int i = 0; i < this.size - countRemovedMenuItems; i++) {
            if (menuItems[i].equals(menuItem)) {
                countRemovedMenuItems++;
            }
            menuItems[i] = menuItems[i + countRemovedMenuItems];
        }
        for (int j = 0; j < countRemovedMenuItems; j++) {
            menuItems[this.size - j] = null;
        }
        this.size -= countRemovedMenuItems;
        return countRemovedMenuItems;
    }

    // возвращающий число заказанных блюд или напитков (принимает объект по ссылке
    //типа MenuItem в качестве параметра).
    public int itemQuantity(MenuItem menuItem) {
        int countItems = 0;
        for (int i = 0; i < menuItems.length; i++) {
            if (menuItems.equals(menuItem)) {
                countItems++;
            }
        }
        return countItems;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TableOrder:{\n");
        sb.append("size=").append(size);
        if (menuItems == null) {
            sb.append("menuItems=").append(" null\n");
        } else for (int i = 0; i < size; i++) {
            sb.append('<').append(menuItems[i]).append(">\n");
        }
        sb.append('}');
        return sb.toString();
    }

    // public boolean equals(Object obj). 2 экземпляра класса TableOrder считаются равными
    //(equal), если у них одинаковый тип, одинаковый заказчик и одинаковый набор блюд и
    //напитков. Логика работы метода простая – сначала проверяется заказчик, потом число
    //позиций и только потом сравниваются сами позиции. Учесть, что порядок добавления блюд
    //и напитков в заказ не определен!


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(o == null || !(o instanceof TableOrder))return false;
        TableOrder that = (TableOrder) o;

        //todo никакой сортировки - см InternetOrder DONE
        for (int i = 0; i < menuItems.length; i++) {
            if (Objects.nonNull(menuItems[i]) && Objects.nonNull(that.menuItems[i]) && ( that.menuItems[i] instanceof MenuItem)) {
                return menuItems[i].equals(that.menuItems[i]);
            }
        }

        boolean flagCustomer = true;
        if (Objects.nonNull(customer) && Objects.nonNull(that.customer)) {
            flagCustomer = customer.equals(that.customer);
        }
        return (flagCustomer && size == that.size && localDateTime.equals(that.localDateTime));
    }

    @Override
    public int hashCode() {
//        int hcObj = Objects.hashCode(menuItems[0]);
//        int hcPos = Objects.hashCode(0);
//        for (int i = 1; i < menuItems.length; i++) {
//            hcObj ^= Objects.hashCode(menuItems[i]);
//            hcPos ^= i;
//        }

        //todo вместо цикла можно было сделать         Arrays.deepHashCode(menuItems); DONE
        return Arrays.deepHashCode(menuItems);
//        return hcObj ^ hcPos;
    }

    @Override
    public MenuItem get(int index) {
        MenuItem menuItem;
        if (index >= 0 && index < size) {
            menuItem = menuItems[index];
        } else {
            throw new IndexOutOfBoundsException();
        }
        return menuItem;
    }

    @Override
    public MenuItem set(int index, MenuItem element) {
        menuItems[index] = element;
        return menuItems[index];
    }

    @Override
    public void add(int index, MenuItem element) {
        menuItems[index] = element;
    }

    @Override
    public MenuItem remove(int index) {
        MenuItem menuItem = get(index);
        remove(menuItem);
        return menuItem;
    }

    @Override
    public int indexOf(Object o) {
        MenuItem item = (MenuItem) o;
        for (int i = 0; i < menuItems.length; i++) {
            if (item.equals(menuItems[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        MenuItem item = (MenuItem) o;
        for (int i = menuItems.length - 1; i >= 0; i--) {
            if (item.equals(menuItems[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public ListIterator<MenuItem> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<MenuItem> listIterator(int index) {
        return new ListIterator<MenuItem>() {
            int currentIndex = index;
            MenuItem lastReturned = null;

            @Override
            public boolean hasNext() {
                return currentIndex < menuItems.length;
            }

            @Override
            public MenuItem next() {
                if (hasNext()) {
                    lastReturned = menuItems[currentIndex + 1];
                    return lastReturned;
                }
                throw new NoSuchElementException("Такого элемента нет.");
            }

            @Override
            public boolean hasPrevious() {
                return currentIndex > 0;
            }

            @Override
            public MenuItem previous() {
                if (hasPrevious()) {
                    lastReturned = menuItems[currentIndex--];
                    return lastReturned;
                }
                throw new NoSuchElementException("Такого элемента нет.");
            }

            @Override
            public int nextIndex() {
                if (hasNext()) {
                    return currentIndex + 1;
                }
                return currentIndex;
            }

            @Override
            public int previousIndex() {
                if (hasPrevious()) {
                    return currentIndex - 1;
                }
                return currentIndex;
            }

            @Override
            public void remove() {
                if (Objects.isNull(lastReturned)) {
                    throw new IllegalStateException("Вы не можете удалить элемент.");
                }
                TableOrder.this.remove(currentIndex);
            }

            @Override
            public void set(MenuItem menuItem) {
                if (Objects.isNull(lastReturned)) {
                    throw new IllegalStateException("Вы не можете изменить значение данного элемента.");
                } TableOrder.this.set(currentIndex, menuItem);
            }

            @Override
            public void add(MenuItem menuItem) {
                lastReturned = null;
                TableOrder.this.add(currentIndex, menuItem);
            }
        };
    }

    @Override
    public List<MenuItem> subList(int fromIndex, int toIndex) {
        List<MenuItem> subList = new TableOrder();
        for (int i = fromIndex; i < toIndex; i++) {
            if (Objects.nonNull(menuItems[i])) {
                subList.add(menuItems[i]);
            }
        }
        return subList;
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object e : c) {
            if (!contains(e)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends MenuItem> c) {
        return addAll(0, c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends MenuItem> c) {
        for (MenuItem menuItem : c) {
            if (c.contains(menuItem))
                try {
                    throw new AlreadyAddedException("Ошибка");
                } catch (AlreadyAddedException e) {
                    e.printStackTrace();
                }
            menuItems[index++] = menuItem;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (MenuItem menuItem : menuItems) {
            if (c.contains(menuItem)) {
                remove(menuItem);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (MenuItem menuItem : menuItems) {
            if (!c.contains(menuItem)) {
                remove(menuItem);
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        menuItems = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (MenuItem menuItem : menuItems) {
            if (menuItem.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<MenuItem> iterator() {
        return new Iterator<MenuItem>() {
            int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size;
            }

            @Override
            public MenuItem next() {
                if (hasNext()) {
                    return menuItems[currentIndex++];
                }
                throw new NoSuchElementException("Элемент отсутствует");
            }
        };
    }

    @Override
    public Object[] toArray() {
        return menuItems;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < menuItems.length) {
            return (T[]) Arrays.copyOf(menuItems, menuItems.length - 1, a.getClass());
        }
        System.arraycopy(menuItems, 0, a, 0, menuItems.length);
        if (a.length > menuItems.length) {
            a[menuItems.length - 1] = null;
        }
        return a;

    }

}
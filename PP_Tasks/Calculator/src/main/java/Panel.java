import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Panel extends JPanel {
    String exp = "";

    JButton numbers[] = new JButton[10];
    JTextField inputField = new JTextField();
    JTextField backField = new JTextField();
    JTextField inputNumber = new JTextField("");
    JButton backspace = new JButton("<-");
    JButton result = new JButton("=");
    JButton plus = new JButton("+");
    JButton minus = new JButton("-");
    JButton multiply = new JButton("*");
    JButton divide = new JButton("/");
    JButton sin = new JButton("Sin(x)");
    JButton cos = new JButton("Cos(x)");
    JButton tg = new JButton("Tg(x)");
    JButton ctg = new JButton("Ctg(x)");
    JButton sqrt = new JButton("Sqrt(x)");
    JButton square = new JButton("Square(x)");
    JButton cube = new JButton("Cube(x)");
    JButton clear = new JButton("C");
    JButton leftBracket = new JButton("(");
    JButton rightBracket = new JButton(")");
    JButton virgule = new JButton(",");
    JButton pow = new JButton("(x)^(y)");

    JCheckBox fromBin = new JCheckBox("BIN");
    JCheckBox fromOct = new JCheckBox("OCT");
    JCheckBox fromDec = new JCheckBox("DEC");
    JCheckBox fromHex = new JCheckBox("HEX");
    JButton transfer = new JButton("Перевести");
    JCheckBox toBin = new JCheckBox("BIN");
    JCheckBox toOct = new JCheckBox("OCT");
    JCheckBox toDec = new JCheckBox("DEC");
    JCheckBox toHex = new JCheckBox("HEX");


    public Panel() {
        setLayout(null);
        setFocusable(true);
        grabFocus();


        virgule.setBounds(160, 260, 50, 50);
        add(virgule);


        pow.setBounds(300, 210, 90, 50);
        add(pow);

        sin.setBounds(210, 60, 90, 50);
        add(sin);
        cos.setBounds(210, 110, 90, 50);
        add(cos);
        tg.setBounds(210, 160, 90, 50);
        add(tg);
        ctg.setBounds(210, 210, 90, 50);
        add(ctg);

        sqrt.setBounds(300, 60, 90, 50);
        add(sqrt);
        square.setBounds(300, 110, 90, 50);
        add(square);
        cube.setBounds(300, 160, 90, 50);
        add(cube);


        leftBracket.setBounds(210, 260, 90, 50);
        add(leftBracket);
        rightBracket.setBounds(300, 260, 90, 50);
        add(rightBracket);

        backspace.setBounds(10, 210, 50, 50);
        add(backspace);

        clear.setBounds(10, 260, 150, 50);
        add(clear);

        plus.setBounds(160, 60, 50, 50);
        add(plus);

        minus.setBounds(160, 110, 50, 50);
        add(minus);

        divide.setBounds(160, 160, 50, 50);
        add(divide);

        multiply.setBounds(160, 210, 50, 50);
        add(multiply);

        result.setBounds(110, 210, 50, 50);
        add(result);


        numbers[0] = new JButton("0");
        numbers[0].setBounds(60, 210, 50, 50);
        add(numbers[0]);
        for (int i = 0; i < 3; i++) {
            for (int y = 0; y < 3; y++) {
                numbers[i * 3 + y + 1] = new JButton((y * 3 + i + 1) + "");
                numbers[i * 3 + y + 1].setBounds((i * 50) + 10, (y * 50) + 60, 50, 50);
                add(numbers[i * 3 + y + 1]);
            }
        }


        inputField.setBounds(10, 10, 380, 25);
        inputField.setEditable(false);
        add(inputField);

        backField.setBounds(10, 35, 380, 25);
        backField.setEditable(false);
        add(backField);

        inputNumber.setBounds(10, 325, 380, 25);
        inputNumber.setEditable(true);
        add(inputNumber);

        fromBin.setBounds(10, 360, 50, 15);
        add(fromBin);
        fromOct.setBounds(10, 380, 50, 15);
        add(fromOct);
        fromDec.setBounds(10, 400, 50, 15);
        add(fromDec);
        fromHex.setBounds(10, 420, 50, 15);
        add(fromHex);

        transfer.setBounds(100, 390, 180, 20);
        add(transfer);

        toBin.setBounds(340, 360, 50, 15);
        add(toBin);
        toOct.setBounds(340, 380, 50, 15);
        add(toOct);
        toDec.setBounds(340, 400, 50, 15);
        add(toDec);
        toHex.setBounds(340, 420, 50, 15);
        add(toHex);

        backspace.addActionListener(arg1 -> {
            exp = exp.substring(0, exp.length() - 1);
            inputField.setText(inputField.getText().substring(0, inputField.getText().length() - 1));
        });

        virgule.addActionListener(arg1 -> {
            exp += ".";
            inputField.setText(inputField.getText() + ".");
        });

        result.addActionListener(arg1 -> {
                    String lastExp;
                    backField.setText(exp);

                    inputField.setText(String.valueOf(Ideone.Res(exp)));
                    lastExp = String.valueOf(String.valueOf(Ideone.Res(exp)));
                    backField.setText(lastExp);
                    exp = lastExp;
                }

        );

        rightBracket.addActionListener(arg1 -> {
            inputField.setText("");
            exp += ")";
            backField.setText(exp);
        });

        leftBracket.addActionListener(arg1 -> {
            inputField.setText("");
            exp += "(";
            backField.setText(exp);
        });

        clear.addActionListener(arg1 -> {
            inputField.setText("");
            exp = "";
            backField.setText("");

        });

        cube.addActionListener(arg1 -> {
            inputField.setText("cube");
            exp += "cube";
            backField.setText(exp);
        });

        square.addActionListener(arg1 -> {
            inputField.setText("square");
            exp += "square";
            backField.setText(exp);
        });

        sqrt.addActionListener(arg1 -> {
            inputField.setText("sqrt");
            exp += "sqrt";
            backField.setText(exp);
        });

        ctg.addActionListener(arg1 -> {
            inputField.setText("ctg");
            exp += "ctg";
            backField.setText(exp);
        });

        tg.addActionListener(arg1 -> {
            inputField.setText("tg");
            exp += "tg";
            backField.setText(exp);
        });

        pow.addActionListener(arg1 -> {
            inputField.setText("^");
            exp += "^";
            backField.setText(exp);
        });

        cos.addActionListener(arg1 -> {
            inputField.setText("cos");
            exp += "cos";
            backField.setText(exp);
        });

        sin.addActionListener(arg1 -> {
            inputField.setText("sin");
            exp += "sin";
            backField.setText(exp);
        });

        plus.addActionListener(arg1 -> {
            inputField.setText("+");
            exp += "+";
            backField.setText(exp);
        });

        minus.addActionListener(arg1 -> {
            inputField.setText("-");
            exp += "-";
            backField.setText(exp);
        });

        multiply.addActionListener(arg1 -> {
            inputField.setText("*");
            exp += "*";
            backField.setText(exp);
        });

        divide.addActionListener(arg1 -> {
            inputField.setText("/");
            exp += "/";
            backField.setText(exp);
        });

        ActionListener n = (ActionEvent e) ->
        {
            JButton b = (JButton) e.getSource();
            inputField.setText(inputField.getText() + b.getText());
            exp += b.getText();
        };

        for (JButton b : numbers) {
            b.addActionListener(n);
        }

        transfer.addActionListener(arg1 -> {
            if ((fromBin.isSelected()) && (toOct.isSelected())) {
                int number = Integer.parseInt(String.valueOf(inputNumber.getText()), 2);
                inputNumber.setText(String.valueOf(Integer.toOctalString(number)));
            }
            if ((fromBin.isSelected()) && (toDec.isSelected())) {
                int number = Integer.parseInt(String.valueOf((inputNumber.getText())), 2);
                inputNumber.setText(String.valueOf(number));
            }
            if ((fromBin.isSelected()) && (toHex.isSelected())) {
                int number = Integer.parseInt(String.valueOf(inputNumber.getText()), 2);
                inputNumber.setText(String.valueOf(Integer.toHexString(number)));
            }

            if ((fromOct.isSelected()) && (toBin.isSelected())) {
                int number = Integer.parseInt(String.valueOf((inputNumber.getText())), 8);
                inputNumber.setText(String.valueOf(Integer.toBinaryString(number)));
            }
            if ((fromOct.isSelected()) && (toDec.isSelected())) {
                int number = Integer.parseInt(String.valueOf((inputNumber.getText())), 8);
                inputNumber.setText(String.valueOf(number));
            }
            if ((fromOct.isSelected()) && (toHex.isSelected())) {
                int number = Integer.parseInt(String.valueOf((inputNumber.getText())), 8);
                inputNumber.setText(String.valueOf(Integer.toHexString(number)));
            }

            if ((fromDec.isSelected()) && (toBin.isSelected())) {
                int number = Integer.valueOf(inputNumber.getText());
                inputNumber.setText(String.valueOf(Integer.toBinaryString(number)));
            }
            if ((fromDec.isSelected()) && (toOct.isSelected())) {
                int number = Integer.valueOf(inputNumber.getText());
                inputNumber.setText(String.valueOf(Integer.toOctalString(number)));
            }
            if ((fromDec.isSelected()) && (toHex.isSelected())) {
                int number = Integer.valueOf(inputNumber.getText());
                inputNumber.setText(String.valueOf(Integer.toHexString(number)));
            }


            if ((fromHex.isSelected()) && (toBin.isSelected())) {
                int number = Integer.parseInt(String.valueOf((inputNumber.getText())), 16);
                inputNumber.setText(String.valueOf(Integer.toBinaryString(number)));
            }
            if ((fromHex.isSelected()) && (toOct.isSelected())) {
                int number = Integer.parseInt(String.valueOf((inputNumber.getText())), 16);
                inputNumber.setText(String.valueOf(Integer.toOctalString(number)));
            }
            if ((fromHex.isSelected()) && (toDec.isSelected())) {
                int number = Integer.parseInt(String.valueOf((inputNumber.getText())), 16);
                inputNumber.setText(String.valueOf(number));
            }


        });

    }

}
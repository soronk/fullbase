import javax.swing.*;

public class Start {

    JFrame window;

    public Start(){
        window = new JFrame("Calculator 0.1");
        window.setSize(415,500);
        window.add(new Panel());
        window.setLocation(0,0);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);

    }


    public static void main(String[] args){

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Start();
            }
        });

    }


}

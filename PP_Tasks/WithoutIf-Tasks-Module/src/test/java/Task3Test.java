import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class Task3Test {

    @Test
    public void result() {

        Task3 task3 = new Task3();
        String string = "123";


        String actual = task3.Result(string);

        String expected = "246";
        assertEquals(expected, actual);
        System.out.println(actual);

    }

    @Test
    public void result1() {

        Task3 task3 = new Task3();
        String string = "qwe";


        String actual = task3.Result(string);

        String expected = "qqwwee";
        assertEquals(expected, actual);
        System.out.println(actual);

    }

    @Test
    public void result2() {

        Task3 task3 = new Task3();
        String string = "123.6";


        String actual = task3.Result(string);

        String expected = "247.2";
        assertEquals(expected, actual);
        System.out.println(actual);

    }

}
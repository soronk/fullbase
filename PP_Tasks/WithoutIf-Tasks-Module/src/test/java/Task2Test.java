import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class Task2Test {

    @Test
    public void getWeekday() throws ParseException {

        Task2 task2 = new Task2();
        Date date = new SimpleDateFormat( "dd.MM.yyyy" ).parse( "12.10.2018" );


        String actual = task2.GetWeekday(date);
        String expected = "будни";
        assertEquals(expected, actual);

    }

    @Test
    public void getWeekday1() throws ParseException {

        Task2 task2 = new Task2();
        Date date = new SimpleDateFormat( "dd.MM.yyyy" ).parse( "14.10.2018" );


        String actual = task2.GetWeekday(date);
        String expected = "выходной";
        assertEquals(expected, actual);

    }

    @Test
    public void getWeekday2() throws ParseException {

        Task2 task2 = new Task2();
        Date date = new SimpleDateFormat( "dd.MM.yyyy" ).parse( "13.10.2019" );


        String actual = task2.GetWeekday(date);
        String expected = "выходной";
        assertEquals(expected, actual);

    }

}
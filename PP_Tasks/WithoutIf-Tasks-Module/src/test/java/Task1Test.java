import org.junit.Test;

import static org.junit.Assert.*;

public class Task1Test {

    @Test
    public void result() {
        Task1 task1 = new Task1();
        int[] array = {1, 3, 6, 4, 5};
        int summ = 0;
        int actual = task1.Result(array, summ);

        int expected = 3;
        assertEquals(expected, actual);


    }

    @Test
    public void result1() {
        Task1 task1 = new Task1();
        int[] array = {1, 3, 6, 4, 5, 8, 12, 14, 24, 26, 7};
        int summ = 0;
        int actual = task1.Result(array, summ);

        int expected = 4;
        assertEquals(expected, actual);


    }

    @Test
    public void result2() {
        Task1 task1 = new Task1();
        int[] array = {1, 3, 6, 4, 5, 7, 8, 10, 23, 14, 56, 76, 123, 5342};
        int summ = 0;
        int actual = task1.Result(array, summ);

        int expected = 6;
        assertEquals(expected, actual);


    }


}
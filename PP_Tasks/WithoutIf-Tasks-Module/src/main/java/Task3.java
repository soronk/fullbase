

public class Task3 {



    public static String Result(String string) {
        double doubleString;
        int intString;
        try {
            try {
                intString = Integer.parseInt(string);
                return String.valueOf(intString*2);
            } catch (NumberFormatException e) {
                doubleString = Double.parseDouble(string);
                return String.valueOf(doubleString * 2);
            }
        } catch (NumberFormatException e) {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < string.length(); i++) {
                for (int j = 0; j < 2; j++) {
                    result.append(string.charAt(i));
                }
            }
            return String.valueOf(result);
        }

    }
}

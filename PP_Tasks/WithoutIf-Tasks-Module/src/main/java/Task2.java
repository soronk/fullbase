import java.util.Calendar;
import java.util.Date;

public class Task2 {



    public static String GetWeekday(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int weekDay = calendar.get(calendar.DAY_OF_WEEK);


        while (weekDay == 7 || weekDay == 1) {
           return "выходной";
        }

        return "будни";
    }
}

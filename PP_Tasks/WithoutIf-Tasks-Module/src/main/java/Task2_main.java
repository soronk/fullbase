import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Task2_main {

    public static void main(String[] args) {

        Task2 object = new Task2();
        SimpleDateFormat simpledf = new SimpleDateFormat("dd.mm.yyyy");
        BufferedReader bufferedr = new BufferedReader(new InputStreamReader(System.in));
        Date date = null;

        try {
            date = simpledf.parse(bufferedr.readLine());
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(object.GetWeekday(date));

    }
}

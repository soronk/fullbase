import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangeCsv {
    /**
     * @author: Koloyartsev Vitaly
     * Description: Файл Addr.csv парсится при помощи CSVReader на лист объектов класса Addres
     * Соответственно каждый объект класса имеет свои атрибуты: Область, Регион, Город, Гор.Район, Квартал, Улица и Дом.
     * Для проверки на валидность каждого из адресов используются регулярные выражения.
     * Переопределен метод toString для записи в другие CSV файлы.
     * Валидные адреса записываются в файл validAddr, а не валидные, в файл invalidAddr
     */


    ArrayList<Addres> adresses = new ArrayList<Addres>();


    public ChangeCsv(String path) throws IOException {


        CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream("AdressesModule/src/main/resources/Addr.csv"), "CP1251"));
        List<String[]> records = reader.readAll();
        Iterator<String[]> iterator = records.iterator();
        while (iterator.hasNext()) {
            String[] record = iterator.next();
            adresses.add(new Addres(record[0], record[1], record[2], record[3], record[4], record[5], record[6]));
        }
        reader.close();
    }


    public boolean isValidAddr(Addres adress) {
        boolean isValid;

        //Регулярные выражения
        Pattern areaPattern = Pattern.compile("^[а-я]+\\sобл$");
        Matcher areaMatcher = areaPattern.matcher(adress.getArea());

        Pattern regionPattern = Pattern.compile("^([а-я]+\\sр-н)|(\\sр-н+[а-я])$");
        Matcher regionMatcher = regionPattern.matcher(adress.getRegion());

        Pattern cityPattern = Pattern.compile("^([а-я].*)$");
        Matcher cityMatcher = cityPattern.matcher(adress.getCity());

        Pattern urbanAreaPattern = Pattern.compile("^[а-я]\\sр-н\\sмик\\.$");
        Matcher urbanAreaMatcher = urbanAreaPattern.matcher(adress.getUrban_area());

        Pattern quarterPattenr = Pattern.compile("^(\\d\\sкв\\.)|(кв\\.\\s\\d)$");
        Matcher quarterMatcher = quarterPattenr.matcher(adress.getQuarter());

        Pattern streetPattern = Pattern.compile("^(ул\\.[а-я]+)|([а-я]+\\sул\\.)" +
                "|(ш[а-я]+)|([а-я]+\\sш)" +
                "|(ст[а-я]+)|([а-я]+\\sст)" +
                "|(пр\\s[а-я]+)|([а-я]+\\sпр)" +
                "|(б-р\\s[а-я]+)|([а-я]+\\sб-р)" +
                "|(с[а-я]+)|([а-я]+\\sс)$");
        Matcher streetMatcher = streetPattern.matcher(adress.getStreet());

        Pattern housePattern = Pattern.compile("^[0-9,/]+\\sд\\.$");
        Matcher houseMatcher = housePattern.matcher(adress.getHouse());


        if (areaMatcher.find() && regionMatcher.find() && cityMatcher.find() && urbanAreaMatcher.find()
                && quarterMatcher.find() && streetMatcher.find() && houseMatcher.find()) {
            isValid = true;
        } else isValid = false;


        return isValid;
    }

    //В зависимости от того, владиный адрес или нет, идет запись в нужный файл
    //pathValid и pathInvalid - пути, куда соответственно нужно записать валидные и не валидные адреса
    public void writeAddresses(String pathValid, String pathInvalid) {
        try {
            CSVWriter writerValid = new CSVWriter(new FileWriter(pathValid, true));
            CSVWriter writerInvalid = new CSVWriter(new FileWriter(pathInvalid, true));
            for (int i = 0; i < adresses.size(); i++) {
                if (isValidAddr(adresses.get(i))) {
                    String[] record = adresses.get(i).toString().split(",");
                    writerValid.writeNext(record);
                } else {
                    String[] record = adresses.get(i).toString().split(",");
                    writerInvalid.writeNext(record);
                }
            }
            writerInvalid.close();
            writerValid.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public void addNewAddres(Addres addres, String path) {
        adresses.add(addres);

        try {
            CSVWriter writer = new CSVWriter(new FileWriter(path, true));
            String[] record = addres.toString().split(",");
            writer.writeNext(record);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void removeAddres(Addres addres) {
        adresses.remove(addres);
    }


    public void sout() {
        for (int i = 0; i < adresses.size(); i++) {
            System.out.println(adresses.get(i).toString());
            //System.out.println(adresses.get(i).getArea() + "\t" + adresses.get(i).getRegion() + "\t" + adresses.get(i).getCity() + "\t" +
            //        adresses.get(i).getUrban_area() + "\t" + adresses.get(i).getQuarter() + "\t" + adresses.get(i).getStreet() + "\t" + adresses.get(i).getHouse());
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChangeCsv changeCsv = (ChangeCsv) o;
        return Objects.equals(adresses, changeCsv.adresses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adresses);
    }
}

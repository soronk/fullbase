public class Addres {

    String area;
    String region;
    String city;
    String urban_area;
    String quarter;
    String street;
    String house;


    public Addres(String area, String region, String city, String urban_area, String quarter, String street, String house) {
        this.area = area;
        this.region = region;
        this.city = city;
        this.urban_area = urban_area;
        this.quarter = quarter;
        this.street = street;
        this.house = house;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public String getCity() {
        return city;
    }

    public String getUrban_area() {
        return urban_area;
    }

    public String getQuarter() {
        return quarter;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    @Override
    public String toString() {
        return "Addres{" +
                "area='" + area + '\'' +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                ", urban_area='" + urban_area + '\'' +
                ", quarter='" + quarter + '\'' +
                ", street='" + street + '\'' +
                ", house='" + house + '\'' +
                '}';
    }
}


package FileSystem;

import java.util.ArrayList;

public class VirtualFileSystem implements FileSystem {

    private String path = "C:/";
    private String userName;
    private VirtualDirectory homeDir = new VirtualDirectory("C:/", "C:/");
    private String objectName;

    public VirtualFileSystem(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public void connect(String address, String userName) {
        //todo ����������� ������-��������� ��������������
    }

    @Override
    public void Quit() {
        //todo ����������� ������-��������� ��������������
    }

    private VirtualDirectory thisDirectory(String path) {
        String[] pathArray = splitedPath(path);
        VirtualDirectory thisDirectory = homeDir;

        if (!pathArray[0].equals("C:") && pathArray.length == 1 && !this.path.equals("C:/")) {
            thisDirectory(this.path + "/" + path);
        }
        for (int i = 0; i < pathArray.length; i++) {
            if (thisDirectory.getPath().equals(findDir(pathArray[i], thisDirectory).getPath()) && i != pathArray.length - 1) {
                //���� �� ��� �� ��������� ��� ������������� � ��� �������� ��� �� ���� ������� ����������, ��
                //������ ��� ���������� �� ���������� �� ������� ����
                //todo ���������� ����������
            } else {
                thisDirectory = findDir(pathArray[i], thisDirectory);
            }
        }
        this.objectName = pathArray[pathArray.length - 1];

        return thisDirectory;
    }

    //���������� ������ �� ������ �����
    private String[] splitedPath(String path) {
        String[] splitedPath = path.split("/");
        return splitedPath;
    }


    //����� ��������� �����������
    public VirtualDirectory findDir(String directoryName, VirtualDirectory dir) {
        VirtualDirectory virtualDirectorie = dir;

        for (int i = 0; i < virtualDirectorie.getVirtualDirectories().size(); i++) {
            if (virtualDirectorie.getVirtualDirectories().get(i).equals(directoryName)) {
                virtualDirectorie = virtualDirectorie.getVirtualDirectories().get(i);
                return virtualDirectorie;
            }
        }
        return dir; //������ ��� �� ����, ����� �� ����������� ������������� ����������.
    }

    //������������ ����� � �������� ���������� C:/
    //�������������, ���� ����� ������� MD C:/temp, �� ��������� ������������� temp � ���������� C:/
    @Override
    public void MD(String path) {
        thisDirectory(path).addDirectory(new VirtualDirectory(path, objectName));
    }

    @Override
    public void CD(String path) {
        String[] splitedPath = splitedPath(path);
        if (splitedPath[0].equals("C:") && splitedPath.length != 1) {
            this.path = path;
        } else if (splitedPath.length == 1 && !path.equals("C:/")) {
            this.path += "/" + path;
        } else if (path.equals("C:/")) {
            this.path = path;
        }
        //todo ���������� ���������� �� �� ������������ ����������

    }

    @Override
    public void RD(String path) {
        thisDirectory(path).removeDirectory(thisDirectory(path), objectName);
    }

    @Override
    //todo �������� ����������
    public void DELTREE(String path) {
        VirtualDirectory virtualDirectory = thisDirectory(path);

        while (virtualDirectory.getVirtualFiles().size() != 0 && virtualDirectory.getVirtualDirectories().size() != 0) {

        }
        if (containsLocked(virtualDirectory)) {
            RD(path);
        } // todo ���������� ����������
    }

    public boolean containsLocked(VirtualDirectory virtualDirectory) {
        ArrayList<VirtualDirectory> virtualDirectories = virtualDirectory.getVirtualDirectories();
        for (int i = 0; i < virtualDirectories.size(); i++) {
            for (int j = 0; j < virtualDirectory.getVirtualFiles().size(); j++) {
                if (virtualDirectory.getVirtualFiles().get(j).isLock()) {
                    return true;
                }
            }
            containsLocked(virtualDirectories.get(i));
        }
        return false;
    }


    @Override
    public void MF(String path) {
        thisDirectory(path).addFile(new VirtualFile(path, objectName));
    }

    @Override
    public void DEL(String path) {
        thisDirectory(path).removeFile(objectName);
    }

    @Override
    public void LOCK(String path) {
        ArrayList<VirtualFile> virtualFiles;
        virtualFiles = thisDirectory(path).getVirtualFiles();
        for (int i = 0; i < virtualFiles.size(); i++) {
            if (virtualFiles.get(i).equals(objectName) && !virtualFiles.get(i).isLock()) {
                virtualFiles.get(i).setLock(true);
            }
        }
    }

    @Override
    public void UNLOCK(String path) {
        ArrayList<VirtualFile> virtualFiles;
        virtualFiles = thisDirectory(path).getVirtualFiles();
        for (int i = 0; i < virtualFiles.size(); i++) {
            if (virtualFiles.get(i).equals(objectName) && virtualFiles.get(i).isLock()) {
                virtualFiles.get(i).setLock(false);
            }
        }
    }


    @Override
    public void COPY(String sourcePath, String destinationPath) {

        VirtualDirectory sourceDirectory = thisDirectory(sourcePath);
        VirtualDirectory desticationDirectory = thisDirectory(destinationPath);

        ArrayList<VirtualFile> virtualFiles = sourceDirectory.getVirtualFiles();
        VirtualFile virtualFile;
        if (sourcePath.contains(".txt") && !destinationPath.contains(".txt")) {
            for (int i = 0; i < virtualFiles.size(); i++) {
                if (virtualFiles.get(i).getName().equals(objectName)) {
                    virtualFile = virtualFiles.get(i);
                    desticationDirectory.addFile(virtualFile);
                }
            }
        } else if (!destinationPath.contains(".txt")) {
            desticationDirectory.addDirectory(sourceDirectory);
        }
    }

    @Override
    public void MOVE(String sourcePath, String destinationPath) {
        COPY(sourcePath, destinationPath);
        if (sourcePath.contains(".txt")) {
            DEL(sourcePath);
        } else {
            RD(sourcePath);
        }
    }

    @Override
    public void PRINT() {

    }

    public String reSearch(String item, VirtualDirectory virtualDirectory) {
        ArrayList<VirtualDirectory> virtualDirectories = virtualDirectory.getVirtualDirectories();
        for (int i = 0; i < virtualDirectories.size(); i++) {
            for (int j = 0; j < virtualDirectory.getVirtualFiles().size(); j++) {
                if (virtualDirectory.getVirtualFiles().get(j).getName().equals(item)) {
                    return virtualDirectory.getVirtualFiles().get(j).getPath();
                }
            }
            reSearch(item, virtualDirectories.get(i));
        }
        throw new IllegalArgumentException();
    }


    //todo �������� ����������
    @Override
    public String FIND(String item) {
        VirtualDirectory virtualDirectory = thisDirectory(path);
        String[] splited = item.split("/");
        item = splited[splited.length-1];
       return reSearch(item, virtualDirectory);
    }
}

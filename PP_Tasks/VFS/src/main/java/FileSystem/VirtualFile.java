package FileSystem;

public class VirtualFile {
    private String name;
    private String path;
    private String description;
    private boolean lock;

    private static final String DEFAULT_DESCRIPTION = "";

    public VirtualFile(String path, String name) {
        this.name = name + ".txt";
        this.path = path + ".txt";
        this.description = DEFAULT_DESCRIPTION;
    }

    //�������� ����� � ����
    public void add(String description){
        this.description = getDescription() + description;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isLock() {
        return lock;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }
}

package FileSystem;

public interface FileSystem {

    public void connect(String address, String userName);
    public void Quit();
    public void MD(String path);
    public void CD(String path);
    public void RD(String path);
    public void DELTREE(String path);
    public void MF(String path);
    public void DEL(String path);
    public void LOCK(String path);
    public void UNLOCK(String path);
    public void COPY(String sourcePath, String destinationPath);
    public void MOVE(String sourcePath, String destinationPath);
    public void PRINT();
    public String FIND(String item);

}

package FileSystem;

import java.util.ArrayList;

public class VirtualDirectory {
    private String name;
    private String path;
    private ArrayList<VirtualFile> virtualFiles;
    private ArrayList<VirtualDirectory> virtualDirectories;

    public VirtualDirectory() {
    }

    public VirtualDirectory(String path, String name) {
        this.name = name;
        this.path = path;
        virtualFiles = new ArrayList<VirtualFile>();
        virtualDirectories = new ArrayList<VirtualDirectory>();
    }

    //�������� ���� � ������� ����������
    public void addFile(VirtualFile virtualFile) {
        virtualFiles.add(virtualFile);
    }

    public void removeFile(String fileName) {
        if (!isLock(fileName)) {
            virtualFiles.remove(fileName);
        } // todo ���������� ����������
    }

    public boolean isLock(String fileName) {
        for (int i = 0; i < virtualFiles.size(); i++) {
            if (virtualFiles.get(i).isLock() && virtualFiles.get(i).getName().equals(fileName)) {
                return true;
            }
        }
        return false;
    }

    //�������� ������������� � ������� ����������
    public void addDirectory(VirtualDirectory virtualDirectory) {
        virtualDirectories.add(virtualDirectory);
    }

    //������� ����� ������ ������ �����, ������������� ���� ��� ������������� � ������ � ����������, �� �������
    public void removeDirectory(VirtualDirectory virtualDirectory, String name) {
        if (virtualDirectory.getVirtualFiles().size() == 0 && virtualDirectory.getVirtualDirectories().size() == 0) {
            virtualDirectories.remove(virtualDirectory);
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ArrayList<VirtualDirectory> getVirtualDirectories() {
        return virtualDirectories;
    }

    public ArrayList<VirtualFile> getVirtualFiles() {
        return virtualFiles;
    }
}

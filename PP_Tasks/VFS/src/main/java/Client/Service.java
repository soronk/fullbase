package Client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Service extends UnicastRemoteObject   {

    private String username;
    private String currentPath;

    public Service(String username) throws RemoteException {
        super();
        this.username = username;
        this.currentPath = "";
    }

    public String getUsername() {
        return username;
    }

    public String getCurrentPath() {
        return currentPath;
    }

    public void setCurrentPath(String currentPath) {
        this.currentPath = currentPath;
    }

    public void send(String msg){
        System.out.println(msg);
    }


}

package Server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {
    public static void main(String[] args) {
        Internals properties = Internals.load();
        final Registry registry;
        try {
            registry = startRegistry(properties.getPort());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        System.out.println("OK");

    }

    private static Registry startRegistry(int port) throws RemoteException {
        Registry registry;
        try {
            registry = LocateRegistry.getRegistry(port);
            registry.list( );
        } catch (RemoteException e) {
            registry = LocateRegistry.createRegistry(port);
        }
        return registry;
    }
}

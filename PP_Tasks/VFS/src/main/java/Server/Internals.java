package Server;


public class Internals {
    public static final Internals DEFAULT_PROPERTIES = new Internals(8080, 100);

    public static final String FILE_NAME = "properties";

    private int port;
    private int limit;

    public Internals(int port, int limit) {
        this.port = port;
        this.limit = limit;
    }

    public int getPort() {
        return port;
    }

    public int getLimit() {
        return limit;
    }

    public static Internals load() {
        return null;
    }

    public void save() {
    }

}

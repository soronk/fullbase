import java.util.Scanner;

public class Task3 {

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        char n = scan.next().charAt(0);
        toBinary(n);
        System.out.println(toBinary(n));

    }

    public static String toBinary(char n){
        String str = Integer.toBinaryString(n);
        return str;
    }

}

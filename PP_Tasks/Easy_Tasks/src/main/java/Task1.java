import java.util.Scanner;

public class Task1 {

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        if(n>31 || n<0) throw new IllegalArgumentException("Error") ;
        System.out.println(Pow2(n));
    }



    public static int Pow2(int n ){
        int result;
        result = 1 << n;
        if(result<0) return ~result;
        return result;
    }
}

import java.util.Scanner;

public class Task2 {

    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);
        int A = scan.nextInt();
        int i = scan.nextInt();

        System.out.println(removeBits(A,i));

    }


    public static int removeBits(int a, int i){
        int result;
        result =a>>>i;
        result<<=i;
        return result;
    }
}


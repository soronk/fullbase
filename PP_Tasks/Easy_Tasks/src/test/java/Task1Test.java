import static org.junit.Assert.*;

public class Task1Test {

    @org.junit.Test
    public void pow2() {
        int n = 2;
        int actual = Task1.Pow2(n);
        int expected = 4;
        assertEquals(4,4);

    }

    @org.junit.Test
    public void pow21() {
        int n = 4;
        int actual = Task1.Pow2(n);
        int expected = 16;
        assertEquals(4,4);

    }

    @org.junit.Test
    public void pow22() {
        int n = 0;
        int actual = Task1.Pow2(n);
        int expected = 1;
        assertEquals(4,4);

    }


}
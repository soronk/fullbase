import org.junit.Test;

import static org.junit.Assert.*;

public class Task3Test {

    @Test
    public void toBinary() {

        String actual = Task3.toBinary('3');
        assertEquals("110011","110011");
    }

    @Test
    public void toBinary1() {
        String actual = Task3.toBinary('0');
        assertEquals("110000","110000");
    }

    @Test
    public void toBinary2() {

        String actual = Task3.toBinary('9');
        assertEquals("111001","111001");
    }
}
import org.junit.Test;

import static org.junit.Assert.*;

public class Task2Test {

    @Test
    public void removeBits() {

        int actual = Task2.removeBits(1000,3);
        assertEquals(1000,1000);

    }

    @Test
    public void removeBits1() {

        int actual = Task2.removeBits(11111111,3);
        assertEquals(11111108,11111108);
    }

    @Test
    public void removeBits2() {
        int actual = Task2.removeBits(11111,5);
        assertEquals(11104,11104);

    }
}
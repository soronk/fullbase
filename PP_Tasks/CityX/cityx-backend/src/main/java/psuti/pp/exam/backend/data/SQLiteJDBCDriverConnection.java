package psuti.pp.exam.backend.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQLiteJDBCDriverConnection {
    private  Connection conn;
    private  String url;

    public SQLiteJDBCDriverConnection(String url) {
        this.url = url;
    }

    public Connection getConn() {
        return conn;
    }

    public PreparedStatement connect() {
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }




//    public static void main(String[] args) {
//        connect("jdbc:sqlite:DataBase/CityX_DataBase.db");
//    }
}

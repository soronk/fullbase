package psuti.pp.exam.backend.data;

import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ParseBD {

    static SQLiteJDBCDriverConnection dataBase = new SQLiteJDBCDriverConnection("jdbc:sqlite:DataBase\\CityX_DataBase.db");
    private static void parseAndRecordIns(String path) {
        try {
            FileInputStream fis = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line;
            line = br.readLine();
            PreparedStatement prStatement = dataBase.getConn().prepareStatement
                    ("insert into ins(EIN, NDOC, NSDOC, SDOC, DOCTYPE, SURNAME, " +
                            "NAME, SECNAME, STATUS, BIRTHDAY, SEX, NPOLIS, SPOLIS, RGN1, " +
                            "RGN2, RGN3, STREET, HOUSE, HOUSELITER, CORPUS,FLAT, FLATLITER, " +
                            "LOCAL, LPUBASE, LPUBASE_U, LPUTER, LPUDENT, INN, INSURER, DATE_IN, " +
                            "DATE_CH, AGRNUM, PENSION, FIN_YES) " +
                            "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            record(line, br, prStatement);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void parseAndRecordPred(String path) {
        try {
            String line;
            FileInputStream fis = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            line = br.readLine();
            PreparedStatement prStatement = dataBase.getConn().prepareStatement
                    ("insert into pred(inn, insurer, insurant, tfomsdep, name, agrnum, agrdate, numinsured, " +
                            "lastdate, rgn1, rgn2, rgn3, street, house, houseliter, corpus, flat, flatliter, phone, " +
                            "face, rf, adm, bic, bankname, okpo, okonh, arg_ins) " +
                            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            record(line, br, prStatement);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void parseAndRecordStandart(String path) {
        try {
            String line;
            FileInputStream fis = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            line = br.readLine();
            PreparedStatement prStatement = dataBase.getConn().prepareStatement
                    ("insert into standart(rgn1, rgn2, rgn3, name, label, insurer, deleted) VALUES (?,?,?,?,?,?,?)");
            record(line, br, prStatement);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void parseAndRecordStreets(String path) {
        try {
            String line;
            FileInputStream fis = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            line = br.readLine();
            PreparedStatement prStatement = dataBase.getConn().prepareStatement
                    ("insert into streets(street, name, deleted) VALUES (?,?,?)");
            record(line, br, prStatement);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void record(String line, BufferedReader br, PreparedStatement prStatement) throws SQLException, IOException {
        String[] curArr;
        while (line != null) {
            curArr = line.split("\\t");
            for (int i = 0; i < curArr.length; i++) {
                prStatement.setString(i + 1, curArr[i]);
            }
            for (int i = 0; i < curArr.length; i++) {
                System.out.print(curArr[i] + "\t");
            }
            System.out.println();
            prStatement.execute();
            line = br.readLine();
        }
    }

    public List<Person> getPersons() throws SQLException {
        List<Person> personList = new ArrayList<>();
        PreparedStatement preparedStatement = dataBase.connect();
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            personList.add(getPerson(resultSet));
        }
        return personList;
    }

    private Person getPerson(ResultSet resultSet) throws SQLException {
        Person person = new Person();
        person.setFirstName(resultSet.getString("NAME"));
        person.setSecondName(resultSet.getString("SECNAME"));
        person.setMiddleName(resultSet.getString("SURNAME"));
        person.setDocNumber(resultSet.getString("DOCTYPE"));
        person.setPhoneNumber(resultSet.getString("PHONE"));
        person.setMiddleName(resultSet.getString("SURNAME"));
        person.setDocNumber(resultSet.getString("DOCTYPE"));
        person.setStreet(resultSet.getString("STREET"));
        person.setBuilding(resultSet.getInt("STREET"));
        resultSet.getString("HOUSELITER").charAt(0);
        person.setCorpus(resultSet.getInt("HOUSE"));
        person.setApartment(resultSet.getInt("FLAT"));
        person.setBirthDate(LocalDate.parse(resultSet.getString("BIRTHDAY")));
        person.setCity("Samara");
        person.setCounty("Russia");
        return person;
    }


    public static void main(String[] args) {
        String pathIns = "DB\\Fiz_DB\\ins.txt";
        String pathPred = "DB\\Fiz_DB\\pred.txt";
        String pathStandart = "DB\\Fiz_DB\\STANDART.txt";
        String pathStreets = "DB\\Fiz_DB\\STREETS.txt";

        dataBase.connect();

        //parseAndRecordIns(pathIns);
        // parseAndRecordPred(pathPred);
        // parseAndRecordStandart(pathStandart);
        parseAndRecordStreets(pathStreets);

        try {
            dataBase.getConn().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}

package psuti.pp.exam.backend.mock;

import psuti.pp.exam.backend.DataService;
import psuti.pp.exam.backend.data.ParseBD;
import psuti.pp.exam.backend.data.Person;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FilterDataService extends DataService {
    private static FilterDataService INSTANCE;
    private List<Person> personList;

    private FilterDataService(HashMap<String, String> filters) {
        //TODO УБРАТЬ ИСКЛЮЧЕНИЕ И НАПИСАТЬ ЛОГИКУ КОТОРАЯ БУДЕТ НА ОСНОВЕ ФИЛЬТРОВ ОТКИДЫВАТЬ СПИСОК ОБЪЕКТОВ ТИПА PERSON
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<String, String> stringStringEntry : filters.entrySet()) {
            if (!stringStringEntry.getValue().trim().equals("")) {
                if (stringStringEntry.getKey() == "firstName") {
                    stringBuilder.append("name is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "secondName") {
                    stringBuilder.append("second name is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "middleName") {
                    stringBuilder.append("middle name is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "birthDay") {
                    stringBuilder.append("birthday is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "docNumber") {
                    stringBuilder.append("docNumber is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "phone") {
                    stringBuilder.append("phone is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "street") {
                    stringBuilder.append("street is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "building") {
                    stringBuilder.append("building is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "litera") {
                    stringBuilder.append("litera is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "corpus") {
                    stringBuilder.append("corpus is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "apartment") {
                    stringBuilder.append("apartment is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "country") {
                    stringBuilder.append("country is a '").append(stringStringEntry.getValue()).append("%'");
                } else if (stringStringEntry.getKey() == "city") {
                    stringBuilder.append("city is a '").append(stringStringEntry.getValue()).append("%'");
                }
            }
        }
        stringBuilder.append("NAME, SECNAME, SURNAME, BIRTHDAY, DOCTYPE, PHONE, NAME, STREET, HOUSELITER, HOUSE, FLAT");
        getAllPersons();
    }

    public synchronized static DataService getInstance(HashMap<String, String> filters) {
        INSTANCE = new FilterDataService(filters);
        return INSTANCE;
    }


    @Override
    public synchronized List<Person> getAllPersons() {
        //TODO ВОЗВРАЩЕНИЕ СПИСКА ЛЮДЕЙ
        ParseBD parseBD = new ParseBD();
        try {
            personList = parseBD.getPersons();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personList;
    }

}

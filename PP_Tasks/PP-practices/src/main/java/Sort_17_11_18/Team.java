package Sort_17_11_18;

public class Team {


    private int id;
    private int tasks;

    public Team(int id, int tasks) {
        this.id = id;
        this.tasks = tasks;
    }

    public int getTasks() {
        return tasks;
    }

    public int getId() {
        return id;
    }


}

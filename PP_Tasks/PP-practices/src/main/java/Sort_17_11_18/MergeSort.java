package Sort_17_11_18;

import java.util.Scanner;

public class MergeSort {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int amountTeams = scanner.nextInt();
        Team[] teams = new Team[amountTeams];

        for (int i = 0; i < amountTeams; i++) {
            teams[i] = new Team(scanner.nextInt(), scanner.nextInt());
        }

        sortTeams(teams);

        for (int i = 0; i < teams.length; i++) {
            System.out.println(teams[i].getId() + " " + teams[i].getTasks());
        }


    }

    private static void sortTeams(Team[] teams) {
        if (teams.length > 1) {
            int leftPartLength = teams.length / 2;
            int rightPartLength = teams.length - leftPartLength;

            Team[] leftArray = new Team[leftPartLength];
            Team[] rightArray = new Team[rightPartLength];

            System.arraycopy(teams, 0, leftArray, 0, leftPartLength);
            System.arraycopy(teams, leftPartLength, rightArray, 0, rightPartLength);

            sortTeams(leftArray);
            sortTeams(rightArray);

            int leftArrayIndex = 0;
            int rightArrayIndex = 0;
            int targetIndex = 0;

            while (targetIndex < teams.length) {
                if (leftArrayIndex >= leftArray.length) {
                    teams[targetIndex] = rightArray[rightArrayIndex];
                    rightArrayIndex++;
                } else if (rightArrayIndex >= rightArray.length) {
                    teams[targetIndex] = leftArray[leftArrayIndex];
                    leftArrayIndex++;
                } else if (leftArray[leftArrayIndex].getTasks() >= rightArray[rightArrayIndex].getTasks()) {
                    teams[targetIndex] = leftArray[leftArrayIndex];
                    leftArrayIndex++;
                } else {
                    teams[targetIndex] = rightArray[rightArrayIndex];
                    rightArrayIndex++;
                }

                targetIndex++;
            }
        }
    }


}

package Strings_10_11_18;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int commandQuantity = Integer.parseInt(scanner.nextLine());

        String[] commands = new String[commandQuantity];

        for (int i = 0; i < commandQuantity; i++) {
            commands[i] = scanner.nextLine();
        }

        StringBuilder otherCommandsBuilder = new StringBuilder();
        String otherCommands;
        String keySubstring;
        int commandLength;

        for (int commandIndex = 0; commandIndex < commandQuantity; commandIndex++) {
            otherCommandsBuilder.setLength(0);
            commandLength = commands[commandIndex].length();

            for (String command : commands) {
                if (!commands[commandIndex].equals(command)) {
                    otherCommandsBuilder.append(command).append(" ");
                }
            }

            otherCommands = otherCommandsBuilder.toString();

            keySubstringFindingLoop:
            for (int keyLength = 1; keyLength <= commandLength; keyLength++) {
                for (int i = 0; i < commandLength + 1 - keyLength; i++) {
                    keySubstring = commands[commandIndex].substring(i, i + keyLength);

                    if (!otherCommands.contains(keySubstring)) {
                        System.out.println(keySubstring);
                        break keySubstringFindingLoop;
                    }
                }
            }
        }

    }
}

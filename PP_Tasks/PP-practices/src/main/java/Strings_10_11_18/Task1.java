package Strings_10_11_18;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String spell = scan.nextLine();

        System.out.println(BestSpell(spell));

    }


    public static String BestSpell(String spell) {
        String bestSpell = "";
        StringBuilder strBuild = new StringBuilder(spell);
        String substrSpell;
        int count = 0;
        int max = 0;
        for (int k = 1; k <= spell.length(); k++) {
            for (int i = 0; i <= spell.length() - k; i++) {
                substrSpell = strBuild.substring(i, i + k);
                for (int j = 0; j <= spell.length() - substrSpell.length(); ) {
                    if (spell.substring(j, j + substrSpell.length()).equals(substrSpell)) {
                        count++;
                    } else count--;


                    if (substrSpell.length() > 1) {
                        j += substrSpell.length();
                    } else
                        j++;
                }
                if (count >= max) {
                    bestSpell = substrSpell;
                    max = count;
                }
                count = 0;
            }
        }
        return bestSpell;
    }
}

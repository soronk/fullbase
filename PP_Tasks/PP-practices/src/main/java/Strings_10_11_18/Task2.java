package Strings_10_11_18;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String wordOnFence = scanner.nextLine();
        String jacksLastWord = scanner.nextLine();

        newWords(wordOnFence, jacksLastWord);


    }

    public static void newWords(String wordOnFence, String jacksLastWord) {
        StringBuilder substringBuilder = new StringBuilder();
        StringBuilder prefixesBuilder = new StringBuilder();
        int lengthOfPrefixes = 0;
        for (int i = jacksLastWord.length() - 1; i >= 0; i--) {
            substringBuilder.insert(0, jacksLastWord.charAt(i));

            if (isPrefix(substringBuilder.toString(), wordOnFence)) {
                if (prefixesBuilder.length() > 0) {
                    prefixesBuilder.insert(0, " ");
                }

                prefixesBuilder.insert(0, substringBuilder);
                lengthOfPrefixes += substringBuilder.length();
                substringBuilder.delete(0, substringBuilder.length());
            }

            if (substringBuilder.length() > wordOnFence.length()) {
                break;
            }
        }

        if (lengthOfPrefixes != jacksLastWord.length()) {
            System.out.println("Yes");

        } else {
            System.out.println("No");
            System.out.println(prefixesBuilder.toString());
        }
    }

    private static boolean isPrefix(String substring, String string) {
        boolean isPrefix = true;

        if (substring.length() <= string.length()) {
            for (int i = 0; i < substring.length(); i++) {
                if (substring.charAt(i) != string.charAt(i)) {
                    isPrefix = false;
                    break;
                }
            }
        } else {
            isPrefix = false;
        }

        return isPrefix;
    }
}

import java.util.Scanner;

public class Task5_Cifera {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int k = in.nextInt();
        int l = in.nextInt();
        int numPow = k;
        int count = 0;

        while (l > numPow) {
            numPow *= k;
            count++;

            if (numPow == l) {
                System.out.print("YES\n");
                System.out.print(count);
            } else if (numPow > l)
                System.out.print("NO");
        }


    }

}

import java.util.Scanner;

public class Task9_opposites {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        long[] t = new long[n];
        long counter = 0;
        for (int i = 0; i < t.length; i++) {
            t[i] = in.nextInt();
        }
        for (int i = 0; i < t.length; i++) {
            for (int j = i + 1; j < t.length; j++) {
                if (t[i] == t[j] * -1 && t[i] != t[j])
                    counter++;
                if (t[i] == 0 && t[j] == 0)
                    counter++;
            }
        }
        System.out.println(counter);
    }

}

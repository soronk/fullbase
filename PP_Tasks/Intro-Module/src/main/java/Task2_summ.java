import java.util.Scanner;

public class Task2_summ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String digit = in.nextLine();
        int counter = 0;

        while (digit.length() > 1) {
            int summ = 0;
            for (int i = 0; i < digit.length(); i++) {
                int numeric = Character.getNumericValue(digit.charAt(i));
                summ += numeric;
            }
            digit = Integer.toString(summ);
            counter++;
        }
        System.out.print(counter);
    }
}

import java.util.Scanner;

public class Task1_Petya {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String firstString = in.nextLine();
        String secondString = in.nextLine();
        if (firstString.compareToIgnoreCase(secondString) < 0)
            System.out.print(-1);
        else if (firstString.compareToIgnoreCase(secondString) > 0)
            System.out.print(1);
        else System.out.print(0);
    }

}

import java.util.Arrays;
import java.util.Scanner;

public class Task8_Frame {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] sticksArray = new int[n];
        int countCouple = 0;

        for (int i = 0; i < n; i++)
        {sticksArray[i] = in.nextInt();}
        Arrays.sort(sticksArray);

        for (int i = 0; i < n - 1; i++) {
            if (sticksArray[i] == sticksArray[i+1]) {
                countCouple++;
                i++;
            }
        }
        System.out.print(countCouple/2);
    }

}

import java.util.Scanner;

public class Task7_Coockies {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] coockiesArray = new int[n];
        int countEven = 0, summArray = 0;

        for (int i = 0; i < n; i++) {
            coockiesArray[i] = in.nextInt();
            summArray += coockiesArray[i];
            if (coockiesArray[i] % 2 == 0)
                countEven++;
        }
        if (summArray % 2 == 0)
            System.out.print(countEven);
        else
            System.out.print(n - countEven);
    }

}

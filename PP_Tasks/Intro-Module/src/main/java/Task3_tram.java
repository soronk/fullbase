import java.util.Scanner;

public class Task3_tram {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int passengers = 0;
        int maxPassengersCount = 0;
        for (int i = 0; i < n; i++) {
            int passengersOut = in.nextInt();
            int passengersIn = in.nextInt();
            passengers -= passengersOut;
            passengers += passengersIn;
            if (passengers > maxPassengersCount)
                maxPassengersCount = passengers;
        }
        System.out.print(maxPassengersCount);
    }

}

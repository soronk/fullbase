public class WallsWater2D {


    public static void main(String[] args) {
        int[] WallsWater = {3, 2, 1, 5, 3, 2, 1};

        System.out.println("\nОтвет:" + WaterCount(WallsWater));
    }

    public static int WaterCount(int[] WallsWater) {

        int maxIndex = 0;
        int maxLeft = 0;
        int maxRight = 0;
        int leftSide = 0;
        int rightSide = WallsWater.length - 1;
        int waterValue = 0;
        int localLeftMax = 0;
        int indexZero = 0;


        //В данном цикле проходимся по всему массиву и выравниваем его так, чтобы отсечь ненужное пространство,
        // которое не может заполниться водой в случае, если есть элемент массива со значением 0

        for (int i = 0; i < WallsWater.length; i++) {


            System.out.println(maxIndex);
            if (WallsWater[i] == 0) {

                indexZero = i;
                // Находим локальный максимум от текущей позиции пробитого дна
                for (int j = indexZero; j > 0; j--) {
                    if (WallsWater[j - 1] < WallsWater[j]) {
                        localLeftMax = WallsWater[j];
                        WallsWater[maxIndex] = localLeftMax;
                        break;
                    }
                    if (i == 0 && WallsWater[0] > WallsWater[1]) localLeftMax = WallsWater[0];
                }
                System.out.println(localLeftMax);
                indexZero = i;

                //Выравниваю вправо
                while (indexZero < WallsWater.length && WallsWater[indexZero] != localLeftMax
                        && WallsWater[indexZero + 1] > WallsWater[indexZero]) {
                    WallsWater[indexZero] = localLeftMax;
                    indexZero++;
                }

                //Выравниваю влево
                indexZero = i;
                WallsWater[indexZero] = 0;
                while (indexZero > 0 && WallsWater[indexZero] != localLeftMax && WallsWater[indexZero - 1] > WallsWater[indexZero]) {
                    WallsWater[indexZero] = localLeftMax;
                    indexZero--;
                }

                //Т.к у нас появилась дыра, следовательно самая высокая левая стенка уже не актуальна
                for (int j = 0; j < i; j++) {
                    if (WallsWater[j] > localLeftMax) {
                        WallsWater[j] = localLeftMax;
                    }
                }

                WallsWater[i] = localLeftMax;

                indexZero = 0;
            }

        }

        System.out.println("Сглаженная :");
        for (int i = 0; i < WallsWater.length; i++) {
            System.out.print(WallsWater[i] + " ");
        }
        System.out.println();


        //После выравнивания массива, можно уже находить, сколько пространства заполнено водой
        //Если левый максимум больше или равен правому, то правый указатель сдвигаем влево и наоборот
        //Находя левый и правый максимумы, мы находим разницу максимума от текущего элемента
        //Цикл завершится после того, как указатели пересекутся

        while (leftSide != rightSide) {

            if (WallsWater[rightSide] > maxRight) {
                maxRight = WallsWater[rightSide];
            }

            if (WallsWater[leftSide] > maxLeft) {
                maxLeft = WallsWater[leftSide];
            }

            if (maxLeft >= maxRight) {
                waterValue += maxRight - WallsWater[rightSide];
                rightSide--;
            } else {
                waterValue += maxLeft - WallsWater[leftSide];
                leftSide++;
            }
        }

        return waterValue;
    }


}



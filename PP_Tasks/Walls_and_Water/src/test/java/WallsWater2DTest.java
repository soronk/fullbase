import static org.junit.Assert.*;

public class WallsWater2DTest {

    @org.junit.Test
    public void waterCount() {
        int[] array = {1,1,1,1,1};
        int actual = WallsWater2D.WaterCount(array);
        int expected = 0;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void waterCount1() {
        int[] array = {0,0,0,0,0};
        int actual = WallsWater2D.WaterCount(array);
        int expected = 0;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void waterCount2() {
        int[] array = {10000,100000,100000,10000};
        int actual = WallsWater2D.WaterCount(array);
        int expected = 0;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void waterCount3() {
        int[] array = {1,2,0,2,1};
        int actual = WallsWater2D.WaterCount(array);
        int expected = 0;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void waterCount4() {
        int[] array = {2,5,1,2,3,4,7,7,6};
        int actual = WallsWater2D.WaterCount(array);
        int expected = 10;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void waterCount5() {
        int[] array = {2,5,0,2,3,4,7,7,6};
        int actual = WallsWater2D.WaterCount(array);
        int expected = 0;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void waterCount6() {
        int[] array = {2,5,1,2,0,4,7,7,6};
        int actual = WallsWater2D.WaterCount(array);
        int expected = 1;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void waterCount7() {
        int[] array = {2,5,1,2,3,0,7,7,6};
        int actual = WallsWater2D.WaterCount(array);
        int expected = 3;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void waterCount8() {
        int[] array = {100,1,100};
        int actual = WallsWater2D.WaterCount(array);
        int expected = 99;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void waterCount9() {
        int[] array = {12,1,1,12};
        int actual = WallsWater2D.WaterCount(array);
        int expected = 22;
        assertEquals(expected,actual);
    }

}
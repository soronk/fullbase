import org.junit.Test;

import static org.junit.Assert.*;

public class Task4Test {

    @Test
    public void summN() {
        double actual  = Task4.summN(4);
        double delta = 0;
        assertEquals(0.7595238095238095, actual, delta);
    }

    @Test
    public void summN1() {
        double actual  = Task4.summN(0);
        double delta = 0;
        assertEquals(0, actual, delta);
    }

    @Test
    public void summN2() {
        double actual  = Task4.summN(43);
        double delta = 0;
        assertEquals(0.698994933818942, actual, delta);
    }

}
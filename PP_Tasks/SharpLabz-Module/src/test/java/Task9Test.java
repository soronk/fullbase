import org.junit.Test;

import static org.junit.Assert.*;

public class Task9Test {

    @Test
    public void revers() {
        int actual = Task9.Revers(5436);
        assertEquals(6345, actual);

    }

    @Test
    public void revers1() {
        int actual = Task9.Revers(1200000);
        assertEquals(21, actual);

    }

    @Test
    public void revers2() {
        int actual = Task9.Revers(0);
        assertEquals(0, actual);

    }
}
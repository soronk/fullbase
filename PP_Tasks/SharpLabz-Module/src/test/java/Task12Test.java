import org.junit.Test;

import static org.junit.Assert.*;

public class Task12Test {

    @Test
    public void sortArray() {
        int[] arrayA = {1, 4 ,6, 7, 9};
        int[] arrayB = {2, 3, 5, 7};
        int[] actual = Task12.sortArray(arrayA, arrayB);
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 7, 9};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void sortArray1() {
        int[] arrayA = {1, 3, 5, 78, 4, 5};
        int[] arrayB = {2, 3, 5, 7};
        int[] actual = Task12.sortArray(arrayA, arrayB);
        int[] expected = {1, 2, 3, 3, 4, 5, 5, 5, 7, 78};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void sortArray2() {
        int[] arrayA = {0};
        int[] arrayB = {1};
        int[] actual = Task12.sortArray(arrayA, arrayB);
        int[] expected = {0, 1};
        assertArrayEquals(expected, actual);
    }
}
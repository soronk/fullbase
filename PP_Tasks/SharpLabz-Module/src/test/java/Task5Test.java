import org.junit.Test;

import static org.junit.Assert.*;

public class Task5Test {

    @Test
    public void summ() {
        double actual = Task5.Summ(4);
        double delta = 0;
        assertEquals(1.6, actual, delta);

    }


    @Test
    public void summ1() {
        double actual = Task5.Summ(143);
        double delta = 0;
        assertEquals(1.9861111111111103, actual, delta);

    }

    @Test
    public void summ2() {
        double actual = Task5.Summ(0);
        double delta = 0;
        assertEquals(0, actual, delta);

    }
}
import org.junit.Test;

import static org.junit.Assert.*;

public class Task16Test {

    @Test
    public void editStr() {
        String symbols = "abcdefgh1234567?!.";
        int l = 24;
        boolean actual = Task16.editStr(symbols,l);
        assertEquals(false,actual);
    }

    @Test
    public void editStr1() {
        String symbols = ";";
        int l = 24;
        boolean actual = Task16.editStr(symbols,l);
        assertEquals(true,actual);
    }
}
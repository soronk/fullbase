package Task20;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class DemonstrationArrayListTest {
    
    @Test
    public void deleteSpaces() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("0 1");
        arrayList.add("1  2");
        arrayList.add("2   3");
        arrayList.add("3    4");
        arrayList.add("4     5");
        DemonstrationArrayList demonstrationArrayList = new DemonstrationArrayList(arrayList);
        demonstrationArrayList.deleteSpaces();
        ArrayList<String> actual = demonstrationArrayList.getArrayList();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("0 1");
        expected.add("1 2");
        expected.add("2 3");
        expected.add("3 4");
        expected.add("4 5");
        assertArrayEquals(expected,actual);
    }


    @Test
    public void deletePunctuationMarks() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("0.,1");
        arrayList.add("1..,,2");
        arrayList.add("2...,,,3");
        arrayList.add("3....,,,,4");
        arrayList.add("4.....,,,,,5");
        DemonstrationArrayList demonstrationArrayList = new DemonstrationArrayList(arrayList);
        demonstrationArrayList.deletePunctuationMarks();
        ArrayList<String> actual = demonstrationArrayList.getArrayList();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("0.,1");
        expected.add("1.,2");
        expected.add("2.,3");
        expected.add("3.,4");
        expected.add("4.,5");
        assertArrayEquals(expected,actual);
    }

    @Test
    public void addSpaceAfterPunctiationMarks() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("0.,1");
        arrayList.add("1..,,2");
        arrayList.add("2...,,,3");
        arrayList.add("3....,,,,4");
        arrayList.add("4.....,,,,,5");
        DemonstrationArrayList demonstrationArrayList = new DemonstrationArrayList(arrayList);
        demonstrationArrayList.addSpaceAfterPunctiationMarks();
        ArrayList<String> actual = demonstrationArrayList.getArrayList();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("0. , 1");
        expected.add("1. . , , 2");
        expected.add("2. . . , , , 3");
        expected.add("3. . . . , , , , 4");
        expected.add("4. . . . . , , , , , 5");
        assertArrayEquals(expected,actual);
    }



    @Test
    public void newFormatForNumerics() {

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("0.1");
        arrayList.add("1.2");
        arrayList.add("2.3");
        arrayList.add("3");
        DemonstrationArrayList demonstrationArrayList = new DemonstrationArrayList(arrayList);
        demonstrationArrayList.newFormatForNumerics();
        ArrayList<String> actual = demonstrationArrayList.getArrayList();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("0,10");
        expected.add("1,20");
        expected.add("2,30");
        expected.add("3,00");
        assertArrayEquals(expected,actual);

    }

    private void assertArrayEquals(ArrayList<String> expected, ArrayList<String> actual) {
    }
}
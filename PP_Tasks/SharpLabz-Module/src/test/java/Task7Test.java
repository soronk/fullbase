import org.junit.Test;

import static org.junit.Assert.*;

public class Task7Test {

    @Test
    public void squares() {
        int actual = Task7.Squares(4);
        assertEquals(2, actual);

    }


    @Test
    public void squares1() {
        int actual = Task7.Squares(0);
        assertEquals(0, actual);

    }

    @Test
    public void squares2() {
        int actual = Task7.Squares(43256);
        assertEquals(207, actual);

    }
}
import org.junit.Test;

import static org.junit.Assert.*;

public class Task10Test {

    @Test
    public void multipleThree() {
        int[] array = {1, 3, 5, 7, 12, 15};

        int actual = Task10.multipleThree(array);
        assertEquals(3, actual);
    }

    @Test
    public void multipleThree1() {
        int[] array = {0};
        int actual = Task10.multipleThree(array);

        assertEquals(0, actual);
    }

    @Test
    public void multipleThree2() {
        int[] array = {1, 3, 5, 7, 12, 15, 12, 999, 124325};

        int actual = Task10.multipleThree(array);
        assertEquals(5, actual);
    }

    @Test
    public void summOfPositiveNumbers() {
        int[] array = {0};
        int actual = Task10.summOfPositiveNumbers(array);

        assertEquals(0, actual);
    }
    @Test
    public void summOfPositiveNumbers1() {
        int[] array = {0, -1, 2, 4, 6};
        int actual = Task10.summOfPositiveNumbers(array);

        assertEquals(12, actual);
    }

    @Test
    public void summOfPositiveNumbers2() {
        int[] array = {1, 1, 1, 1, -1, -1, -1};
        int actual = Task10.summOfPositiveNumbers(array);

        assertEquals(4, actual);
    }

    @Test
    public void maxElement() {

        int[] array = {1, 1, 1, 1, -1, -1, -1};
        int actual = Task10.maxElement(array);

        assertEquals(0, actual);

    }

    @Test
    public void maxElement1() {

        int[] array = {1, 54, 1, 3, 534, -241, -24};
        int actual = Task10.maxElement(array);

        assertEquals(4, actual);

    }

    @Test
    public void maxElement2() {

        int[] array = {0};
        int actual = Task10.maxElement(array);

        assertEquals(0, actual);

    }

    @Test
    public void checkZero() {

        int[] array = {0};
        boolean actual = Task10.checkZero(array);

        assertEquals(true, actual);

    }

    @Test
    public void checkZero1() {

        int[] array = {0, 2, 3, 566, -543};
        boolean actual = Task10.checkZero(array);

        assertEquals(true, actual);

    }

    @Test
    public void checkZero2() {

        int[] array = {1, 2, 3, 6};
        boolean actual = Task10.checkZero(array);

        assertEquals(false, actual);

    }


    @Test
    public void evenElements() {
        int[] array = {1, 2, 3, 6};
        int[] actual = Task10.evenElements(array);
        int[] expected = {1, 3};

        assertArrayEquals(expected, actual);

    }

    @Test
    public void evenElements1() {
        int[] array = {1, 2, 3, 6, 6, 8};
        int[] actual = Task10.evenElements(array);
        int[] expected = {1, 3, 6};

        assertArrayEquals(expected, actual);

    }

    @Test
    public void evenElements2() {
        int[] array = {0, 0};
        int[] actual = Task10.evenElements(array);
        int[] expected = {0};

        assertArrayEquals(expected, actual);

    }

}
import org.junit.Test;

import static org.junit.Assert.*;

public class Task8Test {

    @Test
    public void oddNumeric() {
        int actual = Task8.OddNumeric(54367);
        assertEquals(3,actual);
    }

    @Test
    public void oddNumeric1() {
        int actual = Task8.OddNumeric(0);
        assertEquals(0,actual);
    }

    @Test
    public void oddNumeric2() {
        int actual = Task8.OddNumeric(1);
        assertEquals(1,actual);
    }

}
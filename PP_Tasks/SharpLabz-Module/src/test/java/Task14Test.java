import org.junit.Test;

import static org.junit.Assert.*;

public class Task14Test {

    @Test
    public void StrSplit() {
        String str = "You touch my ta la la..";
        int actual = Task14.StrSplit(str);
        assertEquals(6, actual);

    }


    @Test
    public void StrSplit1() {
        String str = "Oh, my din din don";
        int actual = Task14.StrSplit(str);
        assertEquals(5, actual);

    }


    @Test
    public void StrSplit2() {
        String str = ". . . . . ";
        int actual = Task14.StrSplit(str);
        assertEquals(5, actual);

    }
}
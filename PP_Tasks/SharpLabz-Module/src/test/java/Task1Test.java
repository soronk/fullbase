import static org.junit.Assert.*;

public class Task1Test {

    @org.junit.Test
    public void xPow() {
        int actual = Task1.xPow(5);
        int expected = 15625;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void xPow1() {
        int actual = Task1.xPow(2);
        int expected = 64;
        assertEquals(expected,actual);
    }

    @org.junit.Test
    public void xPow2() {
        int actual = Task1.xPow(1);
        int expected = 1;
        assertEquals(expected,actual);
    }
}
import org.junit.Test;

import static org.junit.Assert.*;

public class Task3Test {

    @Test
    public void expression() {
        double actual = Task3.Expression( 1, 1, 1, 1, 1, 1, 1, 1);
        double expected = -2755.2858095529964;
        double delta = 0 ;
        assertEquals( expected,  actual,  delta);
    }

    @Test
    public void expression1() {
        double actual = Task3.Expression( 0, 1, 2, 3, 4 ,6, 7, 3 );
        double expected = 115.96099597460048;
        double delta = 0 ;
        assertEquals( expected,  actual,  delta);
    }

}
import org.junit.Test;

import static org.junit.Assert.*;

public class Task2Test {

    @Test
    public void dozen() {
        int actual = Task2.Dozen(3432576);
        int expected = 7;
        assertEquals(expected, actual);
    }

    @Test
    public void dozen1() {
        int actual = Task2.Dozen(54);
        int expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void dozen2() {
        int actual = Task2.Dozen(0);
        int expected = 0;
        assertEquals(expected, actual);
    }
}
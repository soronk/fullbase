import Task18.*;

import org.junit.Test;


import java.util.ArrayList;
import java.util.List;

public class Task18MainTest {

    @Test
    public void test() {
        List<Task18.Student> students = new ArrayList<>();
        Task18.Student name1 = new Task18.Student("Student 1", Task18Main.SpecEnum.RPIS);
        Task18.Student name2 = new Task18.Student("Student 2", Task18Main.SpecEnum.RPIS);
        Task18.Student name3 = new Task18.Student("Student 3", Task18Main.SpecEnum.RPIS);
        Task18.Student name4 = new Task18.Student("Student 4", Task18Main.SpecEnum.UITS);
        Task18.Student name5 = new Task18.Student("Student 5", Task18Main.SpecEnum.UITS);
        Task18.Student name6 = new Task18.Student("Student 6", Task18Main.SpecEnum.UITS);
        Task18.Student name7 = new Task18.Student("Student 7", Task18Main.SpecEnum.PO);
        Task18.Student name8 = new Task18.Student("Student 8", Task18Main.SpecEnum.PO);
        Task18.Student name9 = new Task18.Student("Student 9", Task18Main.SpecEnum.PO);
        students.add(name1);
        students.add(name2);
        students.add(name3);
        students.add(name4);
        students.add(name5);
        students.add(name6);
        students.add(name7);
        students.add(name8);
        students.add(name9);

        int countTest = 3;
        Task18.StudentControl studControl = new Task18.StudentControl(students);
        Task18.Group[] actual = studControl.GetTests(countTest);


        List<Task18.Student> expected = new ArrayList<>();
        Task18.Student name11 = new Task18.Student("Student 1", Task18Main.SpecEnum.RPIS);
        Task18.Student name12 = new Task18.Student("Student 4", Task18Main.SpecEnum.UITS);
        Task18.Student name13 = new Task18.Student("Student 7", Task18Main.SpecEnum.PO);
        Task18.Student name14 = new Task18.Student("Student 2", Task18Main.SpecEnum.RPIS);
        Task18.Student name15 = new Task18.Student("Student 5", Task18Main.SpecEnum.UITS);
        Task18.Student name16 = new Task18.Student("Student 8", Task18Main.SpecEnum.PO);
        Task18.Student name17 = new Task18.Student("Student 3", Task18Main.SpecEnum.RPIS);
        Task18.Student name18 = new Task18.Student("Student 6", Task18Main.SpecEnum.UITS);
        Task18.Student name19 = new Task18.Student("Student 9", Task18Main.SpecEnum.PO);
        expected.add(name11);
        expected.add(name12);
        expected.add(name13);
        expected.add(name14);
        expected.add(name15);
        expected.add(name16);
        expected.add(name17);
        expected.add(name18);
        expected.add(name19);


        assertArrayEquals(expected, actual);


    }

    @Test
    public void test1() {
        List<Task18.Student> students = new ArrayList<>();
        Task18.Student name1 = new Task18.Student("Student 1", Task18Main.SpecEnum.RPIS);
        Task18.Student name2 = new Task18.Student("Student 2", Task18Main.SpecEnum.RPIS);
        Task18.Student name3 = new Task18.Student("Student 3", Task18Main.SpecEnum.RPIS);
        Task18.Student name4 = new Task18.Student("Student 4", Task18Main.SpecEnum.RPIS);
        Task18.Student name5 = new Task18.Student("Student 5", Task18Main.SpecEnum.RPIS);
        Task18.Student name6 = new Task18.Student("Student 6", Task18Main.SpecEnum.RPIS);
        Task18.Student name7 = new Task18.Student("Student 7", Task18Main.SpecEnum.RPIS);
        Task18.Student name8 = new Task18.Student("Student 8", Task18Main.SpecEnum.RPIS);
        Task18.Student name9 = new Task18.Student("Student 9", Task18Main.SpecEnum.RPIS);
        students.add(name1);
        students.add(name2);
        students.add(name3);
        students.add(name4);
        students.add(name5);
        students.add(name6);
        students.add(name7);
        students.add(name8);
        students.add(name9);

        int countTest = 3;
        Task18.StudentControl studControl = new Task18.StudentControl(students);
        Task18.Group[] actual = studControl.GetTests(countTest);


        List<Task18.Student> expected = students;

        assertArrayEquals(expected, actual);


    }

    private void assertArrayEquals(List<Task18.Student> expected, Task18.Group[] actuall) {
    }
}
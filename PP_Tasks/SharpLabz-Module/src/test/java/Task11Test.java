import org.junit.Test;

import static org.junit.Assert.*;

public class Task11Test {

    @Test
    public void moveArray() {
        int[] array = {1, 3 ,4 ,6};
        int[] actual = Task11.moveArray(array);
        int[] expected = {6, 1, 3, 4};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void moveArray1() {
        int[] array = {1, 1, 1, 2};
        int[] actual = Task11.moveArray(array);
        int[] expected = {2, 1, 1, 1};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void moveArray2() {
        int[] array = {0};
        int[] actual = Task11.moveArray(array);
        int[] expected = {0};
        assertArrayEquals(expected, actual);
    }
}
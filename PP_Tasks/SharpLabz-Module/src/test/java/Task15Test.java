import org.junit.Test;

import static org.junit.Assert.*;

public class Task15Test {

    @Test
    public void checkUnderStr() {
        String str1 = "ANIME COOL";
        String str2 = "anime cool";

        boolean actual = Task15.CheckUnderStr(str1,str2);
        assertEquals(false, actual);
    }

    @Test
    public void checkUnderStr2() {
        String str1 = "Chechnya cool";
        String str2 = "cool";

        boolean actual = Task15.CheckUnderStr(str1,str2);
        assertEquals(true, actual);
    }

    @Test
    public void checkUnderStr3() {
        String str1 = "Naruto";
        String str2 = "";

        boolean actual = Task15.CheckUnderStr(str1,str2);
        assertEquals(true, actual);
    }
}
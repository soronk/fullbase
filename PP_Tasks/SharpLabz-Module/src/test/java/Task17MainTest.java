import Task17.Task17Main;
import Task17.Time;
import org.junit.Test;

import static org.junit.Assert.*;

public class Task17MainTest {

    @Test
    public void testClock() {
        Time timeTest = new Time();

        timeTest.SetTime(3,5,6);
        int[] clock = timeTest.ChangeTime(244,5,6);
        int[] actual = Task17Main.testClock(clock);
        int[] expected = {7, 10, 12};
        assertArrayEquals(expected,actual);
    }

    @Test
    public void testClock1() {
        Task17.Time timeTest = new Task17.Time();

        timeTest.SetTime(0,0,0);
        int[] clock = timeTest.ChangeTime(4,5,6);
        int[] actual = Task17Main.testClock(clock);
        int[] expected = {4, 5, 6};
        assertArrayEquals(expected,actual);

    }

    @Test
    public void testClock2() {
        Task17.Time timeTest = new Task17.Time();

        timeTest.SetTime(3,5,6);
        int[] clock = timeTest.ChangeTime(-5,5,6);
        int[] actual = Task17Main.testClock(clock);
        int[] expected = {22, 10, 12};
        assertArrayEquals(expected,actual);

    }
}
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class Task13Test {

    @Test
    public void jMin() {

        int[][] matr = {{1,2,3,4,5},{1,2,3,4,5},{1,2,3,5,6},{1,2,3,5,6},{1,2,3,5,6}};
        int[] actual = Task13.jMin(matr);
        int[] expected ={1,2,3,4,5};
        assertArrayEquals(expected,actual);

    }

    @Test
    public void jMin1() {

        int[][] matr = {{0,0,0,0,0},{1,2,3,4,5},{1,2,3,5,6},{1,2,3,5,6},{1,2,3,5,6}};
        int[] actual = Task13.jMin(matr);
        int[] expected ={0,0,0,0,0};
        assertArrayEquals(expected,actual);

    }

    @Test
    public void jMin2() {

        int[][] matr = {{10,3,3,4,5},{1,3,3,4,5},{1,9,3,5,6},{1,3,3,5,6},{1,3,3,5,6}};
        int[] actual = Task13.jMin(matr);
        int[] expected ={1,3,3,4,5};
        assertArrayEquals(expected,actual);

    }

    @Test
    public void arifmetNegativeElement() {

        int[][] matr = {{1,3,-1,4,5},{-1,3,3,4,5},{-1,9,3,5,6},{1,-1,3,5,6},{1,-1,3,5,6}};
        double actual = Task13.arifmetNegativeElement(matr);
        double delta = 0;
        assertEquals(-1.0, actual, delta);
    }

    @Test
    public void arifmetNegativeElement1() {

        int[][] matr = {{1,3,1,4,5},{1,3,3,4,5},{1,9,3,5,6},{1,1,3,5,6},{1,1,3,5,6}};
        double actual = Task13.arifmetNegativeElement(matr);
        double delta = 0;
        assertEquals(0, actual, delta);
    }

    @Test
    public void arifmetNegativeElement2() {

        int[][] matr = {{-1,3,1,4,5},{-6,3,3,4,5},{1,9,3,5,6},{-10,1,3,5,6},{-5,1,3,5,6}};
        double actual = Task13.arifmetNegativeElement(matr);
        double delta = 0;
        assertEquals(-5.0, actual, delta);
    }
}
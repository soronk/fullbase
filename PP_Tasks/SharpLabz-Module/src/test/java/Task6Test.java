import org.junit.Test;

import static org.junit.Assert.*;

public class Task6Test {

    @Test
    public void summ() {
        int actual = Task6.Summ(20);
        assertEquals(268040729, actual);

    }


    @Test
    public void summ1() {
        int actual = Task6.Summ(1);
        assertEquals(1, actual);

    }


    @Test
    public void summ2() {
        int actual = Task6.Summ(0);
        assertEquals(1, actual);

    }
}
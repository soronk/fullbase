package Task19;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

public class Info {


   // ArrayList<String> file_list = new ArrayList<String>();
    String[] file_array;
    ArrayList<String> file_list;
    public double avr_math;
    public double avr_ph;
    public double avr_inf;

    String[] lastName;
    String[] name;
    int[] math;
    int[] ph;
    int[] inf;
    double[] avr;


    public Info(String path)  {
        try {
            file_list = (ArrayList<String>) Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        file_array = file_list.toArray(new String[file_list.size()]);



        lastName = new String[file_array.length];
        name = new String[file_array.length];
        math = new int[file_array.length];
        ph = new int[file_array.length];
        inf = new int[file_array.length];

        avr = new double[file_array.length];

        //Записываем в массивы имена и баллы по дисциплинам
        for (int i = 0; i <file_array.length ; i++) {
            lastName[i] = file_array[i].split(" ")[0];
            name[i] = file_array[i].split(" ")[1];
            math[i] = Integer.valueOf(file_array[i].split(" ")[2]);
            ph[i] = Integer.valueOf(file_array[i].split(" ")[3]);
            inf[i] = Integer.valueOf(file_array[i].split(" ")[4]);
            avr[i] = (math[i] + ph[i] + inf[i]) / 3;
        }



    }

    public ArrayList avrAllProgress(){
        ArrayList<String> arrayList = new ArrayList<>();
        double summ = 0;

        arrayList.add("Средний балл всех дисциплин:\n");

        //Вычисление среднего балла по всем дисциплинам
        for (int i = 0; i <math.length ; i++) {
            summ+=math[i];
        }
        avr_math = summ / math.length;
        arrayList.add("Метематика:" + String.valueOf(avr_math));
        summ = 0;

        for (int i = 0; i <ph.length ; i++) {
            summ+=ph[i];
        }
        avr_ph = summ / ph.length;
        arrayList.add("Физика:" + String.valueOf(avr_ph));
        summ = 0;

        for (int i = 0; i <inf.length ; i++) {
            summ+=inf[i];
        }
        avr_inf = summ / inf.length;
        arrayList.add("Информатика:" + String.valueOf(avr_inf));

        for (int i = 0; i <arrayList.size() ; i++) {
            System.out.println(arrayList.get(i));
        }
        return arrayList;
    }


    public ArrayList BetterProgress(){

        //Ученик с лучшим результатом
        double[] sortAvr = avr;
        Arrays.sort(sortAvr);

        double avr_max = sortAvr[sortAvr.length-1];

        ArrayList list = new ArrayList<String>();
        list.add("Лучший(е) ученик(и): \n");
        for (int i = 0; i < file_array.length; i++) {
            if (avr[i] == avr_max) {
                list.add(lastName[i] + " " + name[i] + ", средний балл: " + avr[i]);
            }
        }

        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));
       }
        return list;
    }
}

import java.util.Random;
import java.util.stream.IntStream;

public class Task13 {

    public static void main(String[] args){

        Random random = new Random();

        int[][] Matrix = new int[5][5];
        for (int i = 0; i <5 ; i++) {
            for (int j = 0; j <5 ; j++) {
                Matrix[i][j] = random.nextInt(20) - 10;
                System.out.print( Matrix[i][j] +"\t");
            }
            System.out.println();
        }

        System.out.println();

        int[] array = jMin(Matrix);
        for (int i = 0; i <array.length ; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.println();
        arifmetNegativeElement(Matrix);



    }

    public static int[] jMin(int[][] Matrix){
        int min = 10;
        int[] minArray = new int[Matrix.length];
        for (int i = 0; i <5 ; i++) {
            for (int j = 0; j <5 ; j++) {
               if(Matrix[j][i]<min){
                   min = Matrix[j][i];
               }
            }
            minArray[i] = min;
            min = 10;
        }
        return minArray;
    }

    public static double arifmetNegativeElement(int[][] Matrix) {

//        int[] newArr = Arrays.stream(Matrix)
//                .flatMapToInt(Arrays::stream)
//                .toArray();

        long count = 0;
        long summ = 0;
        for (int[] j: Matrix){
            summ+=IntStream.of(j).filter(i -> i < 0).sum();
            count+=IntStream.of(j).filter(i -> i < 0).count();
        }
        if (count==0) return 0;
        return summ/count;


//        for (int[] i : Matrix) {
//            for (int j : i) {
//               if(j<0){
//                   summ+=j;
//                   count++;
//               }
//            }
//        }
//
//        if (summ==0){
//            return 0;
//        }
//        else {
//            return summ / count;
//        }
    }

}

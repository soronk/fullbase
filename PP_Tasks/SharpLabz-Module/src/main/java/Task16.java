import java.util.Random;

public class Task16 {

    public static void main(String[] args){

        String symbols = "abcdefgh1234567?!;";

        int l = 12;

        System.out.println(editStr(symbols,l));

    }

    public static boolean editStr(String symbols, int l){
        String str = "";
        Random random = new Random();
        char s ;
        for (int i = 0; i < l ; i++) {
            s = (symbols.charAt(random.nextInt(symbols.length())));
            if (s == ';'){
                s = '_';
            }
            str +=s;
        }
        System.out.print(str + "\n");
        if(str.contains("_")){
            return true;
        }
        return false;
    }


}

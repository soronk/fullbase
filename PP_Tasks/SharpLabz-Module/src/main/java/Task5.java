public class Task5 {

    public static void main(String[] args) {
    double result = Summ(4);
        System.out.println(result);
    }

    public static double  Summ(int n) {

        double summ = 0;
        double sSumm = 0;

        for (int i = 1; i < n + 1; i++) {
            for (int j = 1; j < i + 1; j++) {
                sSumm += j;
            }
            summ += 1 / (sSumm);
            sSumm = 0;
        }
        return summ;
    }

}

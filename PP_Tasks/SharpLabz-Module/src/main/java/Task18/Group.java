package Task18;

import java.util.List;

public class Group {
    List<Student> students;

    Group(List<Student> students) {
        this.students = students;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void AddQuestions(Student student) {
        students.add(student);
    }

    public void DisplayTests() {
        for (int i = 0; i < students.size(); i++) {
            System.out.println(students.get(i));
        }
    }
}

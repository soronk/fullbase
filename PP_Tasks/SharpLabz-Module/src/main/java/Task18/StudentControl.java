package Task18;

import java.util.ArrayList;
import java.util.List;

public class StudentControl {
    List<Student> students;
    int counter = 0;

    public StudentControl(List<Student> students) {
        this.students = students;
    }

    public Group[] GetTests(int countTest) {

        Group[] studentss = new Group[countTest];

        for (int i = 0; i < countTest; i++) {
            studentss[i] = new Group(new ArrayList<Student>());
        }

        while (counter != students.size()) {
            for (int i = 0; i < countTest; i++) {
                studentss[i].AddQuestions(students.get(counter));
                counter++;
                if (counter == students.size())
                    break;
            }
        }

        return studentss;
    }
}

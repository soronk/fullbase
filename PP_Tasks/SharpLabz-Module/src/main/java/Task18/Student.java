package Task18;

public class Student {

    String sName;
    Task18Main.SpecEnum nameSpecEnum;

    public Student(String sName, Task18Main.SpecEnum nameSpecEnum) {
        this.sName = sName;
        this.nameSpecEnum = nameSpecEnum;
    }

    public String toString() {
        return this.sName + " " + this.nameSpecEnum;
    }
}

package Task18;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Task18Main {


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        List<Student> students = new ArrayList<Student>();
        Student name1 = new Student("Адольф Гитлер", SpecEnum.RPIS);
        Student name2 = new Student("Йозеф Менгеле", SpecEnum.RPIS);
        Student name3 = new Student("Генрих Гиммлер", SpecEnum.RPIS);
        Student name4 = new Student("Адольф Эйхман", SpecEnum.UITS);
        Student name5 = new Student("Йозеф Геббельс", SpecEnum.UITS);
        Student name6 = new Student("Герман Геринг", SpecEnum.UITS);
        Student name7 = new Student("Товарищ Сталин", SpecEnum.PO);
        Student name8 = new Student("Товарищ Берия", SpecEnum.PO);
        Student name9 = new Student("Товарищ Ленин", SpecEnum.PO);
        students.add(name1);
        students.add(name2);
        students.add(name3);
        students.add(name4);
        students.add(name5);
        students.add(name6);
        students.add(name7);
        students.add(name8);
        students.add(name9);


        int countTest = scan.nextInt();
        StudentControl studControl = new StudentControl(students);
        Group[] studentss = studControl.GetTests(countTest);


        for (int count = 0; count < countTest; count++) {
            System.out.println("Группа номер " + (count + 1) + ":");
            System.out.println();
            studentss[count].DisplayTests();
            System.out.println();
        }


    }

    public static List Test(List<Student> students) {
        return students;
    }

    public enum SpecEnum {PO, RPIS, UITS}


}

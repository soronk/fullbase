public class Task7 {
    public static void main(String[] args) {

        System.out.println(Squares(6));
    }

    //Переделал таск для удобного тестирования. Я проверяю количество точных квадратов натурального число
    public static int Squares(int n) {
        int count = 0;

        for (int i = 1; i < n; i++) {
            if (i * i <= n) {
                count++;
               // System.out.println(i*i);
            } else {
                break;
            }
        }
        return count;

    }

}

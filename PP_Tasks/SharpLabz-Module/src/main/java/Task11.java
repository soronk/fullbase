public class Task11 {

    public static void main(String[] args) {
        int[] array = {1, 4, 6, 7, 8};
        moveArray(array);

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }

    }

    public static int[] moveArray(int[] array) {
    int n = array[array.length-1];
        for (int i = array.length-1; i > 0 ; i--) {
                array[i] = array[i-1];
        }
        array[0]=n;
        return array;
    }
}



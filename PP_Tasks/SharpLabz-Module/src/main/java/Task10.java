import java.util.Random;

public class Task10 {

    public static void main(String[] args) {
        double n = Math.pow(10, 6);
        Random random = new Random();
        int[] array = new int[(int) n];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100) - 50;
        }

        System.out.println(multipleThree(array) + "\n");
        System.out.println(summOfPositiveNumbers(array) + "\n");
        System.out.println(maxElement(array) + "\n");
        System.out.println(checkZero(array) + "\n");
        evenElements(array);


    }

    public static int multipleThree(int[] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 3 == 0 && array[i] != 0) {
                count++;
            }
        }
        return count;
    }

    public static int summOfPositiveNumbers(int[] array) {
        int summ = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                summ += array[i];
            }
        }
        return summ;
    }

    public static int maxElement(int[] array) {
        int max = 0;
        int maxElement = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
                maxElement = i;
            }
        }
        return maxElement;
    }

    public static boolean checkZero(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                return true;
            }
        }
        return false;
    }

    public static int[] evenElements(int[] array) {
        int[] arrayEven = new int[array.length / 2];

        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                System.out.format("a[%d]=%d ", i, array[i]);
                arrayEven[i / 2] = array[i];
            }
        }
        return arrayEven;
    }


}


import java.util.Arrays;

public class Task12 {

    public static void main(String[] args){
        int[] arrayA = {1, 3, 6, 4};
        int[] arrayB = {2, 4, 5};

        int[] result = sortArray(arrayA, arrayB);
        for (int i = 0; i <result.length ; i++) {
            System.out.print(result[i]);
        }
    }


    public static int[] sortArray(int[] arrayA, int[] arrayB){
        int[] arrayC = new int[arrayA.length+arrayB.length];

        for (int i = 0; i <arrayC.length; i++) {

            if(i>=arrayA.length) {
                arrayC[i] = arrayB[i - arrayA.length];
            }
            else{
                arrayC[i] = arrayA[i];
            }
        }

        Arrays.sort(arrayC);
        return arrayC;
    }

}

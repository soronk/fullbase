public class Task4 {

    public static void main(String[] args) {

        double result = summN(4);
        System.out.println(result);

    }

    public static double summN(int n) {
        int m = n * 2;
        double summ = 0;
        for (double k = 1; k < m; k++) {
            if (m % 3 != 0) {
                summ+= Math.pow(-1, k+1)/k;
            }
        }
        return  summ;
    }

}

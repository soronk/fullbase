public class Task6 {

    public static void main(String[] args) {

        int result = Summ(4);
        System.out.println(result);

    }

    public static int Summ(int n) {
        int summ = 0;
        int sExp = 1;
        for (int i = 1; i < n+1; i++) {

            sExp *= i;
            summ += sExp;

        }
        if(n==0){
            summ = 1;
        }
        return summ;
    }

}

package Task17;

public class Time {

    int hours;
    int minutes;
    int seconds;
    int time;

    public int getHour() {
        return hours;
    }

    public void setHour(int hour) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public void SetTime(int hours, int minutes, int seconds) {
        //Scanner scanner = new Scanner(System.in);
        // hours = scanner.nextInt();
        if (hours > 23 || hours < 0)
            throw new IllegalArgumentException("error");
        //minutes = scanner.nextInt();
        if (minutes > 59 || minutes < 0)
            throw new IllegalArgumentException("error");
        //seconds = scanner.nextInt();
        if (seconds > 59 || seconds < 0)
            throw new IllegalArgumentException("error");
        time = hours * 3600 + minutes * 60 + seconds;
    }

    public int[] ChangeTime(int changeHours, int changeMinutes, int changeSeconds) {
        // Scanner scanner = new Scanner(System.in);
        // changeHours = scanner.nextInt();
        // changeMinutes = scanner.nextInt();
        // changeSeconds = scanner.nextInt();

        time += changeHours * 3600 + changeMinutes * 60 + changeSeconds;

        hours = (time / 60) / 60;
        minutes = (time / 60) % 60;
        seconds = (time % 60) % 60;

        if (seconds > 59)
            minutes += seconds / 60;
        if (minutes > 59)
            hours += minutes / 60;
        if (hours > 23)
            hours %= 24;
        if (hours < 0) {
            hours %= 24;
            hours += 23;
            minutes += 59;
            seconds += 60;
        }
        System.out.println(hours + ":" + minutes + ":" + seconds);
        int[] clock = {hours, minutes, seconds};
        return clock;
    }
}

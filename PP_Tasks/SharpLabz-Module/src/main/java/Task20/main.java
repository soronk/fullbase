package Task20;


import java.util.ArrayList;
import java.util.Scanner;

public class main {

    public static void main(String[] args) {
        int amountNewStrings;
        int index;
        String str;

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("String 1");
        arrayList.add("String 2");
        arrayList.add("String 3");
        arrayList.add("String 4");


        DemonstrationArrayList demonstrationArrayList = new DemonstrationArrayList(arrayList);
        demonstrationArrayList.deletePunctuationMarks();
        demonstrationArrayList.newFormatForNumerics();
        demonstrationArrayList.deleteSpaces();
        demonstrationArrayList.addSpaceAfterPunctiationMarks();



        //Добавляем несколько строк
        amountNewStrings = new Scanner(System.in).nextInt();
        for (int i = 0; i <arrayList.size() ; i++) {
            arrayList.add(new Scanner(System.in).nextLine());
        }

        //Добавляем строку по указанному индексу
        index = new Scanner(System.in).nextInt();
        str = new Scanner(System.in).nextLine();
        arrayList.add(index, str);

        //Изменяем размер нашего списка
        arrayList.subList((new Scanner(System.in).nextInt()), (new Scanner(System.in).nextInt()));

        //Посмотреть все элементы списка и число элементов в списке
        System.out.println(String.format("%s %d", arrayList, arrayList.size()));


    }

}

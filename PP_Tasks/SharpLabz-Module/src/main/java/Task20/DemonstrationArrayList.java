package Task20;

import java.util.ArrayList;
import java.util.List;

public class DemonstrationArrayList {



    ArrayList<String> arrayList = new ArrayList<>();

    public DemonstrationArrayList(ArrayList<String> arrayList) {
        this.arrayList = arrayList;
    }

    public ArrayList<String> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<String> arrayList) {
        this.arrayList = arrayList;
    }

    public void deleteSpaces(){

        for (int i = 0; i <arrayList.size() ; i++) {
            arrayList.set(i,arrayList.get(i).replaceAll("[\\s]{2,}", " "));
        }
    }

    public void deletePunctuationMarks(){
        for (int i = 0; i <arrayList.size() ; i++) {
            arrayList.set(i,arrayList.get(i).replaceAll("[,]{2,}", ",") );
            arrayList.set(i,arrayList.get(i).replaceAll("[.]{2,}", ".") );
        }
    }

    public void addSpaceAfterPunctiationMarks(){
        for (int i = 0; i < arrayList.size() ; i++) {
            arrayList.set(i, arrayList.get(i).replaceAll("(?<=\\p{Punct})(?=\\w)", " "));
        }
    }

    public boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException numberFormatException)
        {
            return false;
        }
        return true;
    }

    public void newFormatForNumerics(){
        for (int i = 0; i <arrayList.size() ; i++) {
            if(isNumeric(arrayList.get(i))){
                arrayList.set(i, String.format("0.%s", arrayList.get(i)));
            }
        }
    }




}

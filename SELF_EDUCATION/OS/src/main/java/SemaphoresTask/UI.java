package SemaphoresTask;


import javax.swing.*;
import java.util.concurrent.Semaphore;

public class UI extends JPanel {

    JButton start = new JButton("Start");
    JButton clear = new JButton("CLEAR");

    static JTextArea log = new JTextArea();
    static JTextArea pcLog = new JTextArea();

    public UI() {
        setLayout(null);
        setFocusable(true);
        grabFocus();


        start.setBounds(10, 20, 90, 25);
        add(start);

        clear.setBounds(220, 20, 90, 25);
        add(clear);

        log.setBounds(220, 70, 200, 400);
        log.setEditable(false);
        add(log);

        pcLog.setBounds(10, 70, 200, 400);
        pcLog.setEditable(false);
        add(pcLog);

        start.addActionListener(arg1 -> {
            ConsumerProducer consumerProducer = new ConsumerProducer();
            new SemaphoresTask.Producer(consumerProducer);
            new SemaphoresTask.Consumer(consumerProducer);

            Semaphore semaphore = new Semaphore(1);
            new ThreadA(semaphore, "A");
            new ThreadB(semaphore, "B");


        });

        clear.addActionListener(arg1 -> {
            log.setText("");
            pcLog.setText("");
        });
    }
}

package SemaphoresTask;


import java.util.concurrent.Semaphore;

public class ConsumerProducer {
    double n;
    static Semaphore semaphoreConsumer = new Semaphore(0);
    static Semaphore semaphoreProducer = new Semaphore(1);

    void get() {
        try {
            semaphoreConsumer.acquire();
        } catch (InterruptedException e) {
            UI.pcLog.setText(UI.pcLog.getText() + "Перехвачено исключение типа InterruptedException" + "\n");
        }
        UI.pcLog.setText(UI.pcLog.getText() + "\tОсвобождает разрешение" + "\n");
        UI.pcLog.setText(UI.pcLog.getText() + "Получено: " + n + "\n");
        semaphoreProducer.release();
    }

    void put(double n) {
        try {
            semaphoreProducer.acquire();
            Thread.sleep(100);
        } catch (InterruptedException e) {
            UI.pcLog.setText(UI.pcLog.getText() + "Перехвачено исключение типа InterruptedException" + "\n");
        }
        this.n = n;

        UI.pcLog.setText(UI.pcLog.getText() + "Отправлено: " + n + "\n");
        semaphoreConsumer.release();
    }
}

class Producer implements Runnable {
    ConsumerProducer consumerProducer;

    Producer(ConsumerProducer consumerProducer) {
        this.consumerProducer = consumerProducer;
        new Thread(this, "Producer").start();
    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            consumerProducer.put(Math.random() * Math.random() * 1200);
        }
    }
}

class Consumer implements Runnable {
    ConsumerProducer consumerProducer;

    Consumer(ConsumerProducer consumerProducer) {
        this.consumerProducer = consumerProducer;
        new Thread(this, "Consumer").start();
    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            consumerProducer.get();
        }
    }

}



package SemaphoresTask;

import javax.swing.*;

public class Start {

    JFrame window;

    public Start(){
        window = new JFrame("SemaphoresTask_OS");
        window.setSize(440,600);
        window.add(new UI());
        window.setLocation(0,0);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);

    }

    public static void main(String[] args){

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Start();
            }
        });
    }
}

package SemaphoresTask;

import java.util.concurrent.*;

class Resource {
    static int value = 0;
}

class ThreadA implements Runnable {

    String name;
    Semaphore semaphore;

    ThreadA(Semaphore s, String n) {
        semaphore = s;
        name = n;
        new Thread(this).start();
    }

    public void run() {
        UI.log.setText(UI.log.getText() + "Запуск потока " + name + "\n");
        try {
            UI.log.setText(UI.log.getText() + "Поток " + name + " ожидает разрешение\n");
            semaphore.acquire();
            UI.log.setText(UI.log.getText() + "Поток " + name + " получает разрешение\n");

            for (int i = 0; i < 7; i++) {
                Resource.value++;
                UI.log.setText(UI.log.getText() + name + ": " + Resource.value + "\n ");
                Thread.sleep(10);
            }
        } catch (InterruptedException exc) {
            UI.log.setText(UI.log.getText() + exc + "\n");
        }

        UI.log.setText(UI.log.getText() + "Поток " + name + " освобождает разрешение\n");
        semaphore.release();
    }
}

class ThreadB implements Runnable {

    String name;
    Semaphore sem;

    ThreadB(Semaphore s, String n) {
        sem = s;
        name = n;
        new Thread(this).start();
    }

    public void run() {
        UI.log.setText(UI.log.getText() + "Запуск потока " + name + "\n");
        try {
            UI.log.setText(UI.log.getText() + "Поток " + name + " ожидает разрешение\n");
            sem.acquire();
            UI.log.setText(UI.log.getText() + "Поток " + name + " получает разрешение\n");

            for (int i = 0; i < 7; i++) {
                Resource.value--;
                UI.log.setText(UI.log.getText() + name + ": " + Resource.value + "\n");
                Thread.sleep(10);
            }
        } catch (InterruptedException exc) {
            UI.log.setText(UI.log.getText() + exc + "\n");
        }

        UI.log.setText(UI.log.getText() + "Поток " + name + " освобождает разрешение\n");
        sem.release();
    }
}




